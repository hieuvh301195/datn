import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  ACCOUNT_SCREEN,
  AVAILABLE_ROOM_SCREEN,
  MEETING_SCHEDULE_SCREEN, NOTIFICATION_SCREEN,
  ONGOING_SCREEN,
} from './RouteName';
import AccountScreen from '../screens/account/AccountScreen';
import AvailableRoomScreen from '../screens/available_room/AvailableRoomScreen';
import MeetingScheduleScreen from '../screens/meeting_schedule/MeetingScheduleScreen';
import OnGoingScreen from '../screens/ongoing/OnGoingScreen';
import NotificationScreen from '../screens/notification/NotificationScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Colors from '../constants/Colors';

const Tab = createBottomTabNavigator();

export default function BottomTabNavigation() {
  const _getIcon = (route, focused) => {
    let iconName = 'home';
    switch (route.name) {
      case ACCOUNT_SCREEN:
        iconName = 'account-circle';
        break;
      case AVAILABLE_ROOM_SCREEN:
        iconName = 'magnify';
        break;
      case MEETING_SCHEDULE_SCREEN:
        iconName = 'calendar';
        break;
      case ONGOING_SCREEN:
        iconName = 'compass';
        break;
      case NOTIFICATION_SCREEN:
        iconName = 'bell';
        break;
    }
    return focused ? (
      <Icon name={iconName} size={20} color={Colors.PRIMARY_COLOR}/>
    ) : (
      <Icon name={iconName} size={20}/>
    );
  };

  return (
    <Tab.Navigator
      headerMode={'none'}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => _getIcon(route, focused)
      })}
      initialRouteName={ONGOING_SCREEN}
      backBehavior={'none'}
      tabBarOptions={{
        activeTintColor: Colors.PRIMARY_COLOR,
        inactiveTintColor: Colors.PRIMARY_DISABLE_FONT_COLOR,
      }}>
      <Tab.Screen
        name={AVAILABLE_ROOM_SCREEN}
        component={AvailableRoomScreen}
        options={{tabBarLabel: 'Phòng trống'}}
      />
      <Tab.Screen
        name={MEETING_SCHEDULE_SCREEN}
        component={MeetingScheduleScreen}
        options={{tabBarLabel: 'Lịch họp'}}
      />
      <Tab.Screen
        name={ONGOING_SCREEN}
        component={OnGoingScreen}
        options={{tabBarLabel: 'On Going'}}
      />
      <Tab.Screen
        name={NOTIFICATION_SCREEN}
        component={NotificationScreen}
        options={{tabBarLabel: 'Thông báo'}}
      />
      <Tab.Screen
        name={ACCOUNT_SCREEN}
        component={AccountScreen}
        options={{tabBarLabel: 'Tài khoản'}}
      />
    </Tab.Navigator>
  );
}

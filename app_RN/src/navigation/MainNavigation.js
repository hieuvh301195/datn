import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {BOOK_MEETING_SCREEN, BOTTOM_TAB_SCREEN, LOGIN_SCREEN, SPLASH_SCREEN} from './RouteName';
import SplashScreen from '../screens/auth/splash/SplashScreen';
import LoginScreen from '../screens/auth/login/LoginScreen';
import BottomTabNavigation from './BottomNavigation';
import BookScheduleScreen from '../screens/book_meeting/BookScheduleScreen';

const Stack = createStackNavigator();

export default function MainNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name={SPLASH_SCREEN} component={SplashScreen}/>
        <Stack.Screen name={LOGIN_SCREEN} component={LoginScreen}/>
        <Stack.Screen name={BOTTOM_TAB_SCREEN} component={BottomTabNavigation}/>
        <Stack.Screen name={BOOK_MEETING_SCREEN} component={BookScheduleScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

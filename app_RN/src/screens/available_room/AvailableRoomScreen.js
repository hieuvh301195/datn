import React, {Component, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  ScrollView,
  Dimensions, StatusBar,
} from 'react-native';

import ItemAvailableRoom from './ItemAvailableRoom';
import {ComponentLoading} from '../common_component/ComponentLoading';
import {_convertStringToTime} from '../../business_logic/process_time';
import {availableRoomViewModel} from './AvailableRoomViewModel';

const {width, height} = Dimensions.get('window');
const windowWidth = width;
const windowHeight = height;

export default function AvailableRoomScreen(props) {

  const {timeline, data, isLoading, _fetchData} = availableRoomViewModel(props);

  useEffect(() => {
    _fetchData();
  }, []);

  if (isLoading) {
    return (
      <ComponentLoading/>
    );
  }
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.titleArea}>
        <View style={styles.titleAround}>
          <Text style={styles.normalText}>DANH SÁCH PHÒNG HỌP</Text>
        </View>
      </View>
      <FlatList
        data={timeline}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => {
          return (
            <ItemAvailableRoom
              {...props}
              timeSlot={item}
              usedRoom={data?.usedRoom?.filter(_item =>
                (_convertStringToTime(item, null) >= _convertStringToTime(null, _item.startedTime))
                && (_convertStringToTime(item, null) <= _convertStringToTime(null, _item.finishedTime)),
              )}
            />
          );
        }}
      />

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    alignItems: 'center',
    paddingTop: StatusBar.currentHeight,
  },
  titleArea: {
    width: windowWidth,
    height: windowHeight / 14,
    justifyContent: 'center',
  },
  titleAround: {
    width: windowWidth,
    height: windowHeight / 24,
    justifyContent: 'flex-start',
    paddingLeft: 5,
  },
  normalText: {
    color: '#979797',
    fontSize: 15,
    fontFamily: 'NunitoSans',
  },
});

import React, {useState} from 'react';
import {getListMeeting} from '../../api/MeetingApi';

export const availableRoomViewModel = (props) => {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState({usedRoom: null});

  const _fetchData = async () => {
    setLoading(true);
    const response = await getListMeeting();
    setLoading(false);
    setData({usedRoom: response?.data?.data});
  };

  const timeline = [
    '09:00',
    '09:30',
    '10:00',
    '10:30',
    '11:00',
    '11:30',
    '12:00',
    '12:30',
    '13:00',
    '13:30',
    '14:00',
    '14:30',
    '15:00',
    '15:30',
    '16:00',
    '16:30',
    '17:00',
    '17:30',
  ];

  return {timeline, isLoading, data, _fetchData}
};

import React, {Component, PureComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableOpacity, Dimensions,
} from 'react-native';
import {BOOK_MEETING_SCREEN} from '../../navigation/RouteName';

const {width, height} = Dimensions.get('window');

const itemWidth = width * 0.95;
const itemHeight = height / 8;

class Room extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.room, this.props.color && {backgroundColor: this.props.color}]}>
        <Text style={styles.meetingRoom}>
          Phòng họp {this.props.meetingRoom}
        </Text>
      </View>
    );
  }
}

export default class ItemAvailableRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.usedRoom ? this.props.usedRoom : [],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.timeSlotArea}>
          <Text style={styles.timeSlot}>{this.props.timeSlot}</Text>
        </View>
        <View style={styles.roomsFlex}>
          {
            this.state.data.map((item, index) => {
              return (
                <Room
                  meetingRoom={item.location.name.slice(10)}
                  color={
                    (item.location.name.slice(10) === 'be' || item.location.name.slice(10) === 'bé') ? '#0077CC' :
                      (item.location.name.slice(10) === 'lon' || item.location.name.slice(10) === 'lớn') ? '#6979F8' : '#3095B5'
                  }
                />
              );
            })
          }
          {
            (this.state.data.length < 2) ? <TouchableOpacity
              style={styles.buttonAddAvailable}
              onPress={() => {
                (this.props.navigation.navigate(BOOK_MEETING_SCREEN));
              }}
            >
              <Text style={{color: '#FFF'}}>+</Text>
            </TouchableOpacity> : null
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: itemHeight,
    width: itemWidth,
    backgroundColor: '#FFF',
    borderBottomWidth: 0.5,
    borderColor: '#949494',
    flexDirection: 'row',
  },
  roomsFlex: {
    width: itemWidth - (itemHeight * 0.7),
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  timeSlotArea: {
    width: itemHeight * 0.7,
    minHeight: itemHeight,
    borderRightWidth: 0.5,
    borderColor: '#949494',
    justifyContent: 'center',
  },
  timeSlot: {
    fontSize: 17,
    fontFamily: 'NunitoSans',
  },
  roomArea: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  room: {
    height: itemHeight / 3,
    backgroundColor: '#0077cc',
    marginLeft: 10,
    borderRadius: (itemHeight / 3) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingHorizontal: 12,
    marginVertical: 5,
  },
  meetingRoom: {
    fontSize: 12,
    fontFamily: 'NunitoSans',
    color: '#FFF',
  },
  buttonAddAvailable: {
    width: itemHeight / 3,
    height: itemHeight / 3,
    backgroundColor: '#BE52F2',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonAddUnavailable: {
    width: itemHeight / 3,
    height: itemHeight / 3,
    backgroundColor: '#e2c8f2',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

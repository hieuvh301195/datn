import React, {useState} from 'react';
import {getListMeeting} from '../../api/MeetingApi';
import {fakeMeetings} from '../../constants/FakeData';

export const onGoingViewModel = (props) => {
  const [isLoading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [data, setData] = useState(fakeMeetings);
  const [itemTodayMeetingShow, setItemTodayMeetingShow] = useState(2);
  const [itemOtherMeetingShow, setItemOtherMeetingShow] = useState(4);

  const _fetchData = async () => {
    setLoading(true);
    const response = await getListMeeting();
    setData(response?.data?.data);
    setLoading(false);
  };

  const _expandTodayMeetingShow = () => setItemTodayMeetingShow(itemTodayMeetingShow === 2 ? 999 : 2);

  const _expandOtherMeetingShow = () => setItemOtherMeetingShow(itemOtherMeetingShow === 4 ? 999 : 4);

  return {
    isLoading,
    refresh,
    data,
    itemTodayMeetingShow,
    itemOtherMeetingShow,
    _fetchData,
    _expandTodayMeetingShow,
    _expandOtherMeetingShow,
  };
};

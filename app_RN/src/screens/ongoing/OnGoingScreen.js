import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView, FlatList, RefreshControl, StatusBar,
} from 'react-native';
import {fakeMeetings} from '../../constants/FakeData';
import ItemIncomingMeeting from '../common_component/ItemIncomingMeeting';
import ItemScheduleMeeting from '../common_component/ItemScheduleMeeting';
import {_addZero, _calculateTotalTimeMeeting, _getCurrentDay} from '../../business_logic/process_time';
import {getListMeeting} from '../../api/MeetingApi';
import {ComponentLoading} from '../common_component/ComponentLoading';
import {room} from '../../constants/CacheRoom';
import {onGoingViewModel} from './OnGoingViewModel';

const {width, height} = Dimensions.get('window');
const fakeData = fakeMeetings;

export default function OnGoingScreen() {
  const {
    isLoading,
    refresh,
    data,
    itemTodayMeetingShow,
    itemOtherMeetingShow,
    _fetchData,
    _expandTodayMeetingShow,
    _expandOtherMeetingShow,
  } = onGoingViewModel();

  useEffect(() => _fetchData(), []);

  return (
    (data !== [])
      ? <View style={styles.container}>
        {isLoading && <ComponentLoading/>}
        <ScrollView
          contentContainerStyle={styles.scrollViewContainer}
          refreshControl={
            <RefreshControl
              refreshing={refresh}
              onRefresh={_fetchData}
            />
          }>
          <View style={styles.titleArea}>
            <View style={styles.titleAround}>
              <Text style={styles.normalText}>LỊCH HỌP SẮP TỚI</Text>
            </View>
          </View>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              if (index < itemTodayMeetingShow) {
                return (
                  <ItemIncomingMeeting
                    startTimeHour={
                      _addZero(new Date(item?.startedTime).getHours())
                    }
                    startTimeMinute={
                      _addZero(new Date(item?.startedTime).getMinutes())
                    }
                    endTimeHour={
                      _addZero(new Date(item?.finishedTime).getHours())
                    }
                    endTimeMinute={
                      _addZero(new Date(item?.finishedTime).getMinutes())
                    }
                    meetingContent={item?.meetingTitle}
                    meetingRoom={room[item?.roomId]}
                    totalTime={
                      _calculateTotalTimeMeeting(item?.startedTime, item?.finishedTime)
                    }
                  />
                );
              } else {
                return (<View/>);
              }
            }}
          />
          {
            (data.length > 2) ?
              <TouchableOpacity
                style={styles.button}
                onPress={_expandTodayMeetingShow}
              >
                <Text
                  style={{
                    fontSize: 9,
                    color: '#6979F8',
                  }}
                >
                  {
                    (itemTodayMeetingShow <= 2) ? 'Xem thêm' : 'Ẩn bớt'
                  }
                </Text>
              </TouchableOpacity> : null
          }
          <View style={styles.titleArea}>
            <View style={styles.titleAround}>
              <Text style={styles.normalText}>{_getCurrentDay()}</Text>
            </View>
          </View>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              if (index < itemOtherMeetingShow) {
                return (
                  <ItemScheduleMeeting
                    startTimeHour={
                      _addZero(new Date(item?.startedTime).getHours())
                    }
                    startTimeMinute={
                      _addZero(new Date(item?.startedTime).getMinutes())
                    }
                    endTimeHour={
                      _addZero(new Date(item?.finishedTime).getHours())
                    }
                    endTimeMinute={
                      _addZero(new Date(item?.finishedTime).getMinutes())
                    }
                    meetingContent={item?.meetingTitle}
                    meetingRoom={room[item?.roomId]}
                  />
                );
              } else {
                return <View/>;
              }
            }}
          />
          {
            (data.length > 4) ?
              <TouchableOpacity
                style={styles.button}
                onPress={_expandOtherMeetingShow}>
                <Text
                  style={{
                    fontSize: 9,
                    color: '#6979F8',
                  }}>
                  {
                    (itemOtherMeetingShow <= 4) ? 'Xem thêm' : 'Ẩn bớt'
                  }
                </Text>
              </TouchableOpacity> : null
          }
        </ScrollView>
      </View>
      : <View style={styles.containerIfBlank}>
        <Text style={styles.normalText}>
          Hiện tại bạn chưa có cuộc họp nào cần tham gia
        </Text>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    paddingTop: StatusBar.currentHeight,
  },
  containerIfBlank: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollViewContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    fontFamily: 'NunitoSans',
  },
  titleArea: {
    width: width,
    height: height / 14,
    justifyContent: 'center',
  },
  titleAround: {
    width: width,
    height: height / 24,
    justifyContent: 'flex-start',
    paddingLeft: 5,
  },
  normalText: {
    color: '#979797',
    fontSize: 13,
    fontFamily: 'NunitoSans',
  },
  button: {
    width: width / 4,
    height: height / 24,
    backgroundColor: '#E5E7FA',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
});

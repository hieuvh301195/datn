import React, {Component} from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Dimensions, StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import ItemNotification from './ItemNotification';
import {ComponentLoading} from '../common_component/ComponentLoading';

const {width, height} = Dimensions.get('window');

const windowWidth = width;
const windowHeight = height;

export default class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingScreen: true,
      listNotification: [
        {
          id: 1,
          startTime: '10:00',
          endTime: '11:00',
          contentMeeting: 'Họp bàn vấn đề nhận lại trang tech.vcclould.vn',
          meetingRoom: 'A',
          totalTime: 60
        },
        {
          id: 2,
          startTime: '11:00',
          endTime: '12:00',
          contentMeeting: 'Daily Meeting - Billing Team',
          meetingRoom: 'VIP',
          totalTime: 60
        },
        {
          id: 3,
          startTime: '14:00',
          endTime: '15:00',
          contentMeeting: 'V/v về vấn đề thanh toán của VCClould',
          meetingRoom: 'A',
          totalTime: 60
        },
        {
          id: 4,
          startTime: '14:00',
          endTime: '15:00',
          contentMeeting: 'Daily Meeting - Billing Team',
          meetingRoom: 'A',
          totalTime: 30
        },
        {
          id: 6,
          startTime: '14:00',
          endTime: '15:00',
          contentMeeting: 'Daily Meeting - Billing Team',
          meetingRoom: 'A',
          totalTime: 30
        },
        {
          id: 7,
          startTime: '14:00',
          endTime: '15:00',
          contentMeeting: 'Daily Meeting - Billing Team',
          meetingRoom: 'A',
          totalTime: 30
        },
        {
          id: 8,
          startTime: '14:00',
          endTime: '15:00',
          contentMeeting: 'Daily Meeting - Billing Team',
          meetingRoom: 'A',
          totalTime: 30
        },
      ],
    }
  }

  async storeNumberOfNotification() {
    try {
      await AsyncStorage.setItem('@hasNotification', this.state.listNotification.length)
    } catch (error) {
      console.log(error.toString())
    }
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        loadingScreen: false
      })
    }, 1000)
  }

  render() {
    if (this.state.loadingScreen) {
      return (
        <ComponentLoading/>
      )
    }
    return (
      (this.state.listNotification.length !== 0) ?
        <ScrollView style={styles.container}>
          <View style={styles.titleArea}>
            <View style={styles.titleAround}>
              <Text style={styles.normalText}>THÔNG BÁO TRONG NGÀY</Text>
            </View>
          </View>
          <ScrollView>
            <FlatList
              data={this.state.listNotification}
              extraData={this.state}
              keyExtractor={(item, index) => item.id}
              renderItem={({item}) => {
                return (
                  <ItemNotification
                    startTime={item.startTime}
                    endTime={item.endTime}
                    meetingContent={item.contentMeeting}
                    meetingRoom={item.meetingRoom}
                    totalTime={item.totalTime}
                  />
                )
              }}
            />
            <View style={styles.deleteNotification}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    listNotification: []
                  });
                  this.render()
                }}>
                <Text style={[styles.normalText, {color: '#707ff6'}]}>Xóa hết thông báo</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ScrollView>
        :
        <View style={styles.containerIfHasNoNotification}>
          <Text style={styles.normalText}>Hiện tại bạn không có thông báo nào</Text>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    paddingTop: StatusBar.currentHeight
  },
  titleArea: {
    width: windowWidth,
    height: windowHeight / 14,
    justifyContent: 'center',
  },
  titleAround: {
    width: windowWidth,
    height: windowHeight / 24,
    justifyContent: 'flex-start',
    paddingLeft: 5
  },
  normalText: {
    color: '#979797',
    fontSize: 15,
    fontFamily: 'RV-Harmonia-Regular'
  },
  containerIfHasNoNotification: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    justifyContent: 'center'
  },
  deleteNotification: {
    width: windowWidth,
    height: windowHeight / 12,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    paddingRight: 10
  }
});

import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableHighlight, Dimensions,
} from 'react-native';
import {Icon} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

const itemWidth = width * 0.95;
const itemHeight = height / 8;
const menuWidth = itemWidth / 2.2;
const menuHeight = itemHeight * 1.86;

export default class ItemIncomingMeeting extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <View style={styles.topLeft}>
            <View style={styles.leftTimeArea}>
              <View style={styles.leftTimeAreaShow}>
                <Text style={styles.formTo}>từ </Text>
                <Text style={[styles.normalText, {fontSize: 16}]}>
                  {this.props.startTimeHour}:{this.props.startTimeMinute}
                </Text>
              </View>
              <View style={styles.leftTimeAreaShow}>
                <Text style={styles.formTo}>đến </Text>
                <Text style={[styles.normalText, {fontSize: 16}]}>
                  {this.props.endTimeHour}:{this.props.endTimeMinute}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.topMiddle}>
            <View style={styles.contentArea}>
              <Text
                style={styles.meetingTitle}
                numberOfLines={2}>
                {this.props.meetingContent}
              </Text>
            </View>
          </View>
          <View style={styles.topRight}>
            <TouchableOpacity>
              <Icon
                name={'more-vert'}
                type={'material'}
                size={26}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bottom}>
          <View style={styles.locationArea}>
            <Icon
              name={'place'}
              type={'material'}
              raise={false}
              size={16}
              color={'#DADADA'}
            />
            <Text style={[styles.normalText, {fontSize: 10}]}>
              {this.props.meetingRoom}
            </Text>
          </View>
          <View style={styles.rightTimeArea}>
            <Icon
              name={'av-timer'}
              type={'material-community'}
              size={16}
              color={'#DADADA'}
            />
            <Text style={[styles.normalText, {fontSize: 10}]}> {this.props.totalTime} phút</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: itemWidth,
    height: itemHeight,
    backgroundColor: '#fdfdfd',
    flexDirection: 'column',
    borderBottomWidth: 0.6,
    borderColor: '#ECEBED',
    fontFamily: 'NunitoSans',
  },
  top: {
    flex: 3,
    flexDirection: 'row',
  },
  bottom: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topLeft: {
    flex: 1.2,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  topMiddle: {
    flex: 4,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  topRight: {
    flex: 0.7,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  leftTimeArea: {
    height: itemHeight / 1.7,
    justifyContent: 'flex-end',
  },
  leftTimeAreaShow: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingRight: 2,
    justifyContent: 'flex-end',
    borderRightWidth: 0.7,
    borderColor: '#ECEBED',
    color: '#979797',
  },
  contentArea: {
    height: itemHeight / 1.7,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: 5,
  },
  iconArea: {
    height: itemHeight / 2,
    width: itemHeight / 2,
    borderRadius: itemHeight / 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  locationArea: {
    width: itemWidth / 3.7,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingRight: 10,
  },
  rightTimeArea: {
    width: itemWidth / 5,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingRight: 10,
  },
  meetingTitle: {
    color: '#6979F8',
    fontSize: 15,
    fontFamily: 'NunitoSans',
  },
  normalText: {
    color: '#979797',
    fontFamily: 'NunitoSans',
  },
  formTo: {
    fontSize: 11,
    color: '#DADADA',
    paddingBottom: 1,
    fontFamily: 'NunitoSans',
  },
  menuItem: {
    flexDirection: 'row',
    height: menuHeight / 4,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 8,
  },
  menu: {
    width: menuWidth,
    height: menuHeight,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  textMenuItem: {
    fontSize: 14,
    color: '#3F3356',
  },
  closeMenuButton: {
    height: menuHeight / 4,
    width: menuHeight / 4,
    backgroundColor: '#EEEEEE',
    borderRadius: menuHeight / 8,
    justifyContent: 'center',
  },
});


import React from 'react';
import {
  Modal,
  Text,
  ActivityIndicator,
  View,
  Dimensions,
  StyleSheet,
} from 'react-native';
import * as Colors from '../../constants/Colors';
import {scaleModerate} from '../../constants/Scale';

const {width, height} = Dimensions.get('window');

export function ComponentLoading(props) {
  const {message, loadingColor} = props;

  return (
    <View style={styles.container}>
      <Modal
        transparent={true}
        animationType={'fade'}
        animated={true}
        visible={true}>
        <View style={[styles.container, {paddingHorizontal: scaleModerate(20)}]}>
          <View style={styles.contentContainer}>
            <ActivityIndicator size={'large'} color={loadingColor || Colors.PRIMARY_COLOR}/>
            <Text style={styles.text}>{message || 'Đang lấy dữ liệu ...'}</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.TRANSPARENT_BLACK_BG,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    width:'100%',
    paddingHorizontal: scaleModerate(10),
    paddingVertical: scaleModerate(20),
    flexDirection: 'row',
    backgroundColor: Colors.WHITE,
    alignItems: 'center',
    borderRadius: scaleModerate(4)
  },
  text: {
    fontSize: scaleModerate(14),
    fontFamily: 'NunitoSans',
    color: Colors.PRIMARY_FONT_COLOR,
    marginLeft: scaleModerate(15)
  },
});

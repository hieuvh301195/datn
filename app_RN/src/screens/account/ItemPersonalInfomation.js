import React, {PureComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
} from 'react-native';

const {width, height} = Dimensions.get('window');

const itemWidth = width * 0.95;
const itemHeight = height / 12;

export default class ItemPersonalInformation extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.labelArea}>
          <Text style={styles.label}>{this.props.label}</Text>
        </View>
        <View style={styles.contentArea}>
          <Text style={styles.content}>{this.props.content}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: itemWidth,
    height: itemHeight,
    backgroundColor: '#FFFFFF',
    shadowOffset: {
      width: 0,
      height: itemHeight,
    },
    shadowColor: '#000',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    borderRadius: 5,
    flexDirection: 'column',
    paddingLeft: 12,
    margin: itemHeight / 5,
    borderWidth: 0.3,
    borderColor: '#949494',
  },
  labelArea: {
    flex: 1,
    justifyContent: 'center',
  },
  contentArea: {
    flex: 1.5,
    justifyContent: 'flex-start',
  },
  label: {
    fontSize: 11,
    fontFamily: 'NunitoSans',
    color: '#D0C9D6',
  },
  content: {
    fontSize: 15,
    fontFamily: 'NunitoSans',
    color: '#949494',
  },
});

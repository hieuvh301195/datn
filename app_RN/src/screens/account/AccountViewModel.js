import React, {useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {LOGIN_SCREEN} from '../../navigation/RouteName';
import {CommonActions} from '@react-navigation/native';

export const accountViewModel = (props) => {
  const [isLoading, setLoading] = useState(false);
  const [userInfo, setUserInfo] = useState({});

  const _getUserInfo = async () => {
    setLoading(true);
    const result = await AsyncStorage.getItem('user_info') || null;
    setLoading(false);
    if (result !== null) {
      const userInfo = JSON.parse(result);
      setUserInfo(userInfo);
    }
  };

  const _logout = async () => {
    await AsyncStorage.removeItem('user_info');
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: LOGIN_SCREEN}],
      }),
    );
  };

  return {isLoading, userInfo, _getUserInfo, _logout};
};

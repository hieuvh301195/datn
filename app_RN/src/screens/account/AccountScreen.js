import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions, StatusBar,
} from 'react-native';
import ItemPersonalInformation from './ItemPersonalInfomation';
import {ComponentLoading} from '../common_component/ComponentLoading';
import {accountViewModel} from './AccountViewModel';

const {width} = Dimensions.get('window');
const windowWidth = width;
const statusBarHeight = StatusBar.currentHeight;

export default function AccountScreen(props) {
  const {isLoading, userInfo, _getUserInfo, _logout} = accountViewModel(props);

  useEffect(() => {
    _getUserInfo()
  }, []);

  if (isLoading) {
    return (
      <ComponentLoading/>
    );
  }
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <Image
          style={styles.userPicture}
          source={{uri: userInfo.photo}}
        />
        <Text style={styles.userName}>{userInfo.name}</Text>
        <ItemPersonalInformation
          label={'Email'}
          content={userInfo.email}
        />
        <ItemPersonalInformation
          label={'Ngày sinh'}
          content={'30-11-1995'}
        />
        <ItemPersonalInformation
          label={'Địa chỉ'}
          content={'9D1 ngõ 190 Lò Đúc'}
        />
        <TouchableOpacity style={styles.alterPersonalInfoArea} onPress={_logout}>
          <Text style={styles.alterPersonalInfo}>Đăng xuất</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    paddingTop: StatusBar.currentHeight,
  },
  scrollViewContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  userPicture: {
    width: windowWidth / 3,
    height: windowWidth / 3,
    marginTop: statusBarHeight,
    marginBottom: statusBarHeight / 2,
    borderRadius: windowWidth / 6,
    borderWidth: 0.6,
    borderColor: '#DADADA',
  },
  userName: {
    fontFamily: 'RV-Harmonia-Regular',
    fontSize: 20,
  },
  alterPersonalInfoArea: {
    margin: statusBarHeight / 3,
  },
  alterPersonalInfo: {
    color: '#6979F8',
    fontSize: 13,
  },
});

import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements';

const {width, height} = Dimensions.get('window')

const itemWidth = width * 0.95;
const itemHeight = 60;
const menuWidth = itemWidth / 2.2;
const menuHeight = itemHeight * 3.5;

export default class ItemScheduleMeeting extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.left}>
          <View style={styles.leftTop}>
            <Text style={styles.meetingTitle}
                  numberOfLines={2}>
              {this.props.meetingContent}
            </Text>
          </View>
          <View style={styles.leftBottom}>
            <View style={styles.locationArea}>
              <Icon
                name={'place'}
                type={'material'}
                size={16}
                color={'#DADADA'}
              />
              <Text style={[styles.normalText, {fontSize: 9}]}>
                {this.props.roomName}
              </Text>
            </View>
            <View style={styles.rightTimeArea}>
              <Icon
                name={'av-timer'}
                type={'material-community'}
                size={16}
                color={'#DADADA'}
              />
              <Text style={[styles.normalText, {fontSize: 9}]}> từ {this.props.startTimeHour}
                :{this.props.startTimeMinute} đến {this.props.endTimeHour}:
                {this.props.endTimeMinute}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.right}>
          <Icon
            name={'more-vert'}
            type={'material'}
            size={26}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: itemWidth,
    height: itemHeight,
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: '#fdfdfd',
    flexDirection: 'row',
    borderBottomWidth: 0.6,
    borderColor: '#ECEBED',
    fontFamily: 'NunitoSans',
  },
  left: {
    flex: 9,
    paddingLeft: 5,
  },
  right: {
    flex: 1,
    justifyContent: 'center',
  },
  leftTop: {
    paddingBottom: 4,
    justifyContent: 'center',
  },
  leftBottom: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  locationArea: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    width: itemWidth / 4,
  },
  rightTimeArea: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  normalText: {
    color: '#979797',
    fontFamily: 'NunitoSans',
  },
  meetingTitle: {
    fontSize: 14,
    color: '#6979F8',
    fontFamily: 'NunitoSans',
  },
  menuItem: {
    flexDirection: 'row',
    height: menuHeight / 4,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 8,
  },
  menu: {
    width: menuWidth,
    height: menuHeight,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  textMenuItem: {
    fontSize: 14,
    color: '#3F3356',
  },
  closeMenuButton: {
    height: menuHeight / 4,
    width: menuHeight / 4,
    backgroundColor: '#EEEEEE',
    borderRadius: menuHeight / 8,
    justifyContent: 'center',
  },
});


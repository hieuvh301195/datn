import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Dimensions,
  ScrollView, StatusBar,
} from 'react-native';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

import ItemScheduleMeeting from './ItemScheduleMeeting';
import {ComponentLoading} from '../common_component/ComponentLoading';
import {LocaleConfig} from 'react-native-calendars';
import {getListMeetingSort} from '../../api/MeetingApi';
import {texts} from '../../constants/Styles';

const {width, height} = Dimensions.get('window');

LocaleConfig.locales['us'] = {
  monthNames: ['Janury', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  monthNamesShort: ['Jan.', 'Feb', 'Mar', 'April', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  dayNames: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  dayNamesShort: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  today: 'Today',
};
LocaleConfig.defaultLocale = 'us';

let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


export default class MeetingScheduleScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingScreen: false,
      datePicker: '',
      markedDates: {},
      listScheduleMeeting: [
        {
          id: 4,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp con Server Management',
          roomName: 'A',
        },
        {
          id: 5,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'V/v về vấn đề thanh toán của VcCloud',
          roomName: 'VIP',
        },
        {
          id: 6,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp bàn nhận lại trang tech.cloud.vn',
          roomName: 'A',
        },
        {
          id: 7,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp con Server Management',
          roomName: 'VIP',
        },
        {
          id: 8,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp con Server Management',
          roomName: 'VIP',
        },
        {
          id: 9,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp con Server Management',
          roomName: 'A',
        },
        {
          id: 10,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp con Server Management',
          roomName: 'A',
        },
        {
          id: 11,
          startedTime: '14:00',
          finishedTime: '15:00',
          meetingTitle: 'Họp bàn nhận lại trang tech.cloud.vn',
          roomName: 'A',
        },
      ],
    };
  }

  UNSAFE_componentWillMount() {
    let a = {};
    let today = new Date();
    let month = (today.getMonth() + 1).toString().length === 1 ? '0' + (today.getMonth() + 1) : today.getMonth() + 1;
    let date = today.getDate().toString().length === 1 ? '0' + today.getDate() : today.getDate();
    let todayStr = date + '-' + month + '-' + today.getFullYear();


    this.setState({
      datePicker: days[today.getDay() + 1].toString() + ', ' + todayStr,
    });

    a[todayStr] = {selected: true, marked: true};
    this.setState({
      markedDates: a,
    });
  }

  async _showDayTest(date) {
    console.log(date);
    let a = {};
    a[date.dateString] = {selected: true, marked: true};
    this.setState({
      markedDates: a,
    });

    let b = new Date(date.timestamp);

    this.setState({
      datePicker: days[b.getDay() + 1].toString() + ' ' + date.dateString,
    });
    await this._fetchData(date.dateString);
  }

  async componentDidMount(): Promise<void> {
    // this.setState({isLoading: true});
    const today = new Date();
    const date = today.toISOString().split('T')[0];

    await this._fetchData(date);
  }

  _fetchData = async (date) => {
    const response = await getListMeetingSort(date);
    // this.setState({isLoading: false});
    if (response?.data?.data) {
      this.setState({
        listScheduleMeeting: response?.data?.data,
      });
    }
  };

  render() {
    if (this.state.loadingScreen) {
      return (
        <ComponentLoading/>
      );
    }

    return (
      <ScrollView style={styles.container}>
        <View style={{width: width, backgroundColor: 'red'}}>
          <Calendar
            horizontal={true}
            // Callback which gets executed when visible months change in scroll view. Default = undefined
            onVisibleMonthsChange={(months) => {
              console.log('now these months are visible', months);
            }}
            // Max amount of months allowed to scroll to the past. Default = 50
            pastScrollRange={50}
            // Max amount of months allowed to scroll to the future. Default = 50
            futureScrollRange={50}
            // Enable or disable scrolling of calendar list
            scrollEnabled={false}
            // Enable or disable vertical scroll indicator. Default = false
            showScrollIndicator={true}
            theme={{
              backgroundColor: '#FFF',
              calendarBackground: '#FFF',
              textSectionTitleColor: '#000',
              selectedDayBackgroundColor: '#6979F8',
              selectedDayTextColor: '#FFFFFF',
              todayTextColor: '#00adf5',
              dayTextColor: '#525252',
              textDisabledColor: '#d9e1e8',
              dotColor: '#6979F8',
              selectedDotColor: '#ffffff',
              arrowColor: 'orange',
              monthTextColor: '#3095B5',
              indicatorColor: 'red',
              textDayFontWeight: '300',
              textMonthFontWeight: 'bold',
              textDayHeaderFontWeight: '300',
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16,
              dayHeaderFontWeight: 'bold',
            }}

            markedDates={this.state.markedDates}

            onDayPress={(dateString) => this._showDayTest(dateString)}
          >
          </Calendar>
        </View>

        <View style={{width: width, backgroundColor: 'blue'}}>
          <View style={{
            flex: 1,
            backgroundColor: '#E5E5E5',
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{color: '#979797', fontWeight: 'bold'}}>{this.state.datePicker}</Text>
          </View>
          <View style={{flex: 5, backgroundColor: '#FFF', alignItems: 'center', paddingBottom: 15}}>
            <FlatList
              ListEmptyComponent={<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={texts.normal}>Bạn không có cuộc họp nào vào thời gian này</Text>
              </View>}
              data={this.state.listScheduleMeeting}
              extraData={this.state}
              keyExtractor={(item, index) => item.id}
              renderItem={({item, index}) => {
                return (
                  <ItemScheduleMeeting
                    startedTime={item.startedTime}
                    finishedTime={item.finishedTime}
                    meetingContent={item.meetingTitle}
                    roomName={item.roomName}
                  />
                );
              }
              }
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#b0c4de',
    paddingTop: StatusBar.currentHeight
  },
});

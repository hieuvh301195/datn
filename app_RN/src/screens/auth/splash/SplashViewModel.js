import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

export const splashViewModel = (props) => {
  const dispatch = useDispatch();
  const language = useSelector(state => state.commonReducer?.language);
  useEffect(() => _getDeviceLanguage(), []);
}

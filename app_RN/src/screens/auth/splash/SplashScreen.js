import React, {useState, useCallback, useEffect} from 'react';
import {Dimensions, Image, ImageBackground, NativeModules, Platform, StyleSheet, Text, View} from 'react-native';
import {scaleModerate} from '../../../constants/Scale';
import {texts} from '../../../constants/Styles';
import {getString} from '../../../constants/String';
import {changeLanguage} from '../../../actions/CommonActions';
import {useDispatch, useSelector} from 'react-redux';
import {CHANGE_LANGUAGE} from '../../../actions/ActionsType';
import {BOTTOM_TAB_SCREEN, LOGIN_SCREEN} from '../../../navigation/RouteName';
import AsyncStorage from '@react-native-community/async-storage';

const {width, height} = Dimensions.get('screen');
const appLogo = require('../../../assets/images/splash/logoNL.png');
const splashBackground = require('../../../assets/images/splash/splash.jpg');
const logoAndSlogan = require('../../../assets/images/splash/Logo.png');

export default function SplashScreen(props) {



  const _changeLanguage = useCallback((language) => changeLanguage(language), [language]);
  // dispatch({type: CHANGE_LANGUAGE, language});
  const _getDeviceLanguage = () => {
    let locale;
    if (Platform.OS === 'ios') {
      const {} = NativeModules.SettingsManager?.settings;
      locale = NativeModules.SettingsManager?.settings?.AppleLocale;
      locale = locale || NativeModules.SettingsManager.settings.AppleLanguages[0];
    } else if (Platform.OS === 'android') {
      locale = NativeModules.I18nManager?.localeIdentifier;
    }
    const language = locale?.includes('en') ? 'en' : 'vn';
    console.log('language here = ', language);
    changeLanguage(language);
  };

  useEffect(() => {
    function _checkUserInfo() {
      try {
        AsyncStorage.getItem('user_info').then((result) => {
          if (result === null) {
            _goToNextPage(LOGIN_SCREEN);
          } else {
            _goToNextPage(BOTTOM_TAB_SCREEN);
          }
        });
      } catch (exception) {
        console.log(exception);
      }
    }

    _checkUserInfo();
  }, []);


  const _goToNextPage = (pageName) => setTimeout(() => props.navigation.navigate(pageName), 2000);

  return (
    <ImageBackground style={styles.container} source={splashBackground}>
      <View style={styles.sloganArea}>
        <Image source={logoAndSlogan} style={styles.logo} resizeMode={'contain'}/>
      </View>
      {/*<Text>{getString(language, 'APP_SLOGAN')}</Text>*/}
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
    height,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sloganArea: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: scaleModerate(30),
  },
  logo: {
    width: '100%',
  },
});

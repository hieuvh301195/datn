import React, {useState} from 'react';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';
import {BOTTOM_TAB_SCREEN} from '../../../navigation/RouteName';

const webClientId = '758512433972-e18m0oqideu1e9qvl527ioavgp4b0nq4.apps.googleusercontent.com';

GoogleSignin.configure({
  webClientId,
  offlineAccess: true,
  forceConsentPrompt: true,
});
export const loginViewModel = (props) => {
  const [loadingLogin, setLoadingLogin] = useState(false);

  const _signIn = async () => {
    try {
      setLoadingLogin(true);
      await GoogleSignin.hasPlayServices();
      if (await GoogleSignin.isSignedIn() === true) {
        await GoogleSignin.signOut();
      }
      let userInfo = await GoogleSignin.signIn();
      console.log('user info = ', userInfo);
      userInfo = JSON.stringify(userInfo.user);
      await AsyncStorage.setItem('user_info', userInfo);
      let googleToken = await GoogleSignin.getTokens();
      console.log('accessToken ', googleToken.accessToken);
      setLoadingLogin(false);
      props.navigation.navigate(BOTTOM_TAB_SCREEN);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.error('sign in canceled: ', error.code);
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.error('sign in in progress: ', error.code);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.error('play services is not available: ', error.code);
      } else {
        console.error('Google Signin error: ', error.toString());
        console.error(statusCodes);
      }
      setLoadingLogin(false)
    }
  };

  return {loadingLogin, _signIn}
}

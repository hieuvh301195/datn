import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image, ActivityIndicator,
} from 'react-native';
import * as Colors from '../../../constants/Colors';
import {scaleModerate, scaleVertical} from '../../../constants/Scale';
import {texts} from '../../../constants/Styles';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import AsyncStorage from "@react-native-community/async-storage";
import {BOTTOM_TAB_SCREEN} from '../../../navigation/RouteName';
import {loginViewModel} from './LoginViewModel';

const {width, height} = Dimensions.get('window');
const imageBackground = require('../../../assets/images/common/login_background.png');
const googleIcon = require('../../../assets/images/common/google_icon.png');

export default function LoginScreen(props) {
  const {loadingLogin, _signIn} = loginViewModel(props);

  return (
    <ImageBackground style={styles.container} source={imageBackground}>
      <View style={styles.contentArea}>
        <View style={styles.contentAreaTop}>
          <Text style={styles.vMeeting}>nMeeting</Text>
          <View style={styles.divideLine}/>
          <Text style={styles.builtByVCerForVCer}>built by Nexters for Nexters</Text>
        </View>
        <View style={[styles.contentAreaBottom]}>
          <GoogleButton onPress={_signIn} loading={loadingLogin}/>
        </View>

      </View>
    </ImageBackground>
  );
}

const GoogleButton = (props) => {
  const {onPress, loading} = props;
  if (loading) {
    return (
      <View style={styles.button}>
        <ActivityIndicator color={Colors.WHITE} size={'small'}/>
      </View>
    );
  }
  return (
    <TouchableOpacity
      onPress={onPress}
      delayPressIn={0}
      style={styles.button}
      activeOpacity={0.7}>

      <Image source={googleIcon} style={styles.googleIcon}/>
      <Text style={[texts.normal, {color: Colors.WHITE, fontWeight: 'bold'}]}>
        Đăng nhập với NextTech
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width,
    height,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: scaleModerate(25),

  },
  button: {
    width: '100%',
    borderRadius: scaleModerate(4),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scaleModerate(20),
    backgroundColor: Colors.GOOGLE_COLOR,
    paddingVertical: scaleVertical(15),
  },
  googleIcon: {
    width: scaleModerate(16),
    height: scaleModerate(16),
    marginRight: scaleModerate(20),
  },
  contentArea: {
    height: height / 2.9,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  contentAreaTop: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  contentAreaBottom: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 10,
  },
  vMeeting: {
    fontSize: 38,
    color: 'white',
    fontWeight: 'bold'
  },
  divideLine: {
    width: width / 6,
    borderTopWidth: 1,
    borderTopColor: '#979797',
  },
  builtByVCerForVCer: {
    fontSize: scaleModerate(13),
    color: '#fafafa',
    fontFamily: 'RV-Harmonia-Bold',
    marginTop: 5,
  },
  buttonLoginText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'RV-Harmonia-Bold',
  },

});

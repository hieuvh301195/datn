const vn = {
  APP_SLOGAN: 'Một sản phẩm của\nNgân Lượng',
};

const en = {
  APP_SLOGAN: 'A production of\nNgan Luong',
};

export const getString = (language, contentCode) => {
  switch (language) {
    case 'vn':
      return vn[contentCode];
    case 'en':
      return en[contentCode];
    default:
      return vn[contentCode];
  }
};

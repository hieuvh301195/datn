export const fakeMeetings = [
    {
        id: 1,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 2,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn triển khai dự án Alepay',
        roomId:1,
        room:{
            id:2,
            roomName: 'Phòng khách'
        }
    },
    {
        id: 3,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp team marketing',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng đổi'
        }
    },
    {
        id: 4,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp team kỹ thuật',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 5,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn giao công việc của Hiếu',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 6,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Bàn lại một số luồng của dự án thu học phí',
        roomId:1,
        room:{
            id:2,
            roomName: 'Phòng khách'
        }
    },
    {
        id: 7,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn nhận lại trang NganLuong.vn',
        roomId:1,
        room:{
            id:2,
            roomName: 'Phòng khách'
        }
    },
    {
        id: 8,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 9,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:2,
            roomName: 'Phòng khách'
        }
    },
    {
        id: 10,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:2,
            roomName: 'Phòng khách'
        }
    },
    {
        id: 11,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 12,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 13,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 14,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    },
    {
        id: 15,
        startedTime: 1595671394118,
        finishedTime: 1595671394118,
        meetingTitle: 'Họp bàn việc golive app Ngân Lượng',
        roomId:1,
        room:{
            id:1,
            roomName: 'Phòng học'
        }
    }
];

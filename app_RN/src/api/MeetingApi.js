import {callGetApi} from './CommonApi';
import {LIST_MEETING, MEETING} from './URL';
import AsyncStorage from '@react-native-community/async-storage';

export const getListMeeting = async () => {
  return await callGetApi(LIST_MEETING);
};

export const getMeetingById = async (meetingId) => {
  const URL = MEETING + '/' + meetingId;
  return await callGetApi(URL);
};

export const getListMeetingSort = async (date) => {
  const userInfoStore = await AsyncStorage.getItem('user_info') || null;
  console.log('userInfoStore = ', userInfoStore);
  if (userInfoStore) {
    const email = "hieu.vh301195@gmail.com";
    const URL = LIST_MEETING + `?date=${date}&email=${email}`;
    return await callGetApi(URL)
  }
};

import axios from 'axios';

export const callApi = async (method, url, data) => {
  const config = {method, url, data};
  console.log("config api = ", config);
  try{
    const response = await axios(config);
    console.log(response);
    console.log(JSON.stringify(response));
    return response;
  }catch (error) {
    console.log(error);
    console.log(JSON.stringify(error));
    return {code: -1, message: 'There is some error happened'};
  }
};

export const callGetApi = async (url, data) => {
  return await callApi('GET', url, data);
};
export const callPostApi = async (url, data) => {
  return await callApi('POST', url, data);
};
export const callPutApi = async (url, data) => {
  return await callApi('PUT', url, data);
};
export const callDeleteApi = async (url, data) => {
  return await callApi('DELETE', url, data);
};

import {callGetApi} from './CommonApi';
import {EMPLOYEE, LIST_EMPLOYEE} from './URL';

export const getListEmployee = async () => {
  return await callGetApi(LIST_EMPLOYEE);
};

export const getEmployeeById = async (id) => {
  const url = EMPLOYEE + `/${id}`;
  return await callGetApi(url);
};

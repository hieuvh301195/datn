const BASE_URL = "https://infinite-spire-44186.herokuapp.com/"

export const LIST_MEETING = BASE_URL + 'meeting_list';
export const MEETING = BASE_URL + "meeting";
export const LIST_EMPLOYEE = BASE_URL + "employee_list"
export const EMPLOYEE = BASE_URL + "employee"

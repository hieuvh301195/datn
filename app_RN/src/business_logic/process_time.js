export const _addZero = (time) => {
  if (time < 10) {
    return '0' + time;
  } else {
    return time;
  }
};

export const _calculateTotalTimeMeeting = (start, end) => {
  let endTime = new Date(end);
  let startTime = new Date(start);
  let differentMilliseconds = (endTime - startTime);
  let differentHours = Math.floor((differentMilliseconds % 86400000) / 3600000);
  let differentMinutes = Math.round(((differentMilliseconds % 86400000) % 3600000) / 60000);
  if (differentHours >= 1) {
    return differentMinutes + differentHours * 60;
  } else {
    return differentMinutes;
  }
};

export const _getCurrentDay = () => {
  let today = new Date();
  const listDay = ['Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy', 'Chủ nhật'];
  let day = today.getDay();
  let date = today.getDate();
  if (date < 10) {
    date = '0' + date;
  }
  let month = today.getMonth() + 1;
  if (month < 10) {
    month = '0' + month;
  }
  return listDay[day - 1].toString() + ', ' + date + '/' + month;
};

export const _convertStringToTime = (justHourMinutes, fullTime) => {
  if (justHourMinutes) {
    let date = new Date();
    let today = new Date().toISOString().split('T')[0];
    let timeStr = today + 'T' + justHourMinutes + ':00';
    return new Date(timeStr);
  }
  if (fullTime) {
    return new Date(fullTime);
  }
};

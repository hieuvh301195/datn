import 'package:app/navigation/BottomTabNavigator.dart';
import 'package:app/screen/auth/LoginScreen.dart';
import 'package:app/screen/auth/SplashScreen.dart';

import 'package:flutter/material.dart';


class MainNavigator{
  MainNavigator._();

  static const String ROOT = '/';
  static const String SPLASH_SCREEN = "/SPLASH_SCREEN";
  static const String LOGIN_SCREEN = "/LOGIN_SCREEN";
  static const String BOTTOM_TAB_NAVIGATION = "/BOTTOM_TAB_NAVIGATION";
  static const String ON_GOING_SCREEN = "/ON_GOING_SCREEN";
  static const String AVAILABLE_ROOM_SCREEN = "/AVAILABLE_ROOM_SCREEN";
  static const String SCHEDULE_SCREEN = "/SCHEDULE_SCREEN";
  static const String NOTIFICATION_SCREEN = "/NOTIFICATION_SCREEN";
  static const String ACCOUNT_SCREEN = "/ACCOUNT_SCREEN";
  static const String CREATE_MEETING_SCREEN = "/CREATE_MEETING_SCREEN";

  static final routes = <String, WidgetBuilder>{
    ROOT: (BuildContext context) => SplashScreen(),
    LOGIN_SCREEN: (BuildContext context) => LoginScreen(),
    BOTTOM_TAB_NAVIGATION: (BuildContext context) => BottomTabNavigator()
  };
}

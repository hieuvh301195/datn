class RouteName{
  static const String SPLASH_SCREEN = "SPLASH_SCREEN";
  static const String LOGIN_SCREEN = "LOGIN_SCREEN";
  static const String BOTTOM_TAB_NAVIGATION = "BOTTOM_TAB_NAVIGATION";
  static const String ON_GOING_SCREEN = "ON_GOING_SCREEN";
  static const String AVAILABLE_ROOM_SCREEN = "AVAILABLE_ROOM_SCREEN";
  static const String SCHEDULE_SCREEN = "SCHEDULE_SCREEN";
  static const String NOTIFICATION_SCREEN = "NOTIFICATION_SCREEN";
  static const String ACCOUNT_SCREEN = "ACCOUNT_SCREEN";
  static const String CREATE_MEETING_SCREEN = "CREATE_MEETING_SCREEN";
}
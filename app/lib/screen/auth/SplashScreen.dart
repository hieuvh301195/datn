import 'dart:async';

import 'package:app/navigation/MainNavigator.dart';
import 'package:flutter/material.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final String urlBackground = "assets/images/splash/splash_background.png";
  final String urlLogo = "assets/images/splash/logo.png";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _startTime();
  }

  _startTime() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, _navigationPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Splash Screen'),
      ),
    );
  }

  void _navigationPage() {
    Navigator.of(context).pushReplacementNamed(MainNavigator.LOGIN_SCREEN);
  }
}


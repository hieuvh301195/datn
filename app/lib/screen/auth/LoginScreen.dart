import 'package:app/navigation/MainNavigator.dart';
import 'package:app/res/images/Images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  void _moveToHome() {
    Navigator.pushNamed(context, MainNavigator.BOTTOM_TAB_NAVIGATION);
  }

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  _renderGoogleButton() {
    return Padding(
      padding: EdgeInsets.only(top: 40.0),
      child: Container(
      height: 50.0,
      decoration: BoxDecoration(
        color: Colors.deepOrange,
      ),
    ), );
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    print('render login');
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Images.LOGIN_BACKGROUND_IMAGE),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 32.0, right: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "nMeeting",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 35.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Divider(
                        height: 1,
                        color: Colors.white,
                        endIndent: 200,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "built by Nexters for Nexters",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                      _renderGoogleButton()
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

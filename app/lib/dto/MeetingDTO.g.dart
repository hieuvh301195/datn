// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MeetingDTO.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************
const ID = 'id';
const TITLE = "title";
const DESCRIPTION = "description";
const CREATOR_ID = "creator_id";
const IS_PERIODIC = "is_periodic";
const STATE = "state";
const ROOM_ID = "room_id";
const STARTED_TIME = "started_time";
const FINISHED_TIME = "finished_time";
const ROOM = 'room';

MeetingDTO _$MeetingDTOFromJson(Map<String, dynamic> json) {
  return MeetingDTO(
    json[ID] as int,
    json[TITLE] as String,
    json[DESCRIPTION] as String,
    json[CREATOR_ID] as int,
    json[IS_PERIODIC] as bool,
    json[STATE] as int,
    json[ROOM_ID] as int,
    json[STARTED_TIME] == null
        ? null
        : DateTime.parse(json[STARTED_TIME] as String),
    json[FINISHED_TIME] == null
        ? null
        : DateTime.parse(json[FINISHED_TIME] as String),
    json[ROOM] == null
        ? null
        : Room.fromJSON(json[ROOM] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MeetingDTOToJson(MeetingDTO instance) =>
    <String, dynamic>{
      ID: instance.id,
      TITLE: instance.title,
      DESCRIPTION: instance.description,
      CREATOR_ID: instance.creatorId,
      IS_PERIODIC: instance.isPeriodic,
      STATE: instance.state,
      ROOM_ID: instance.roomId,
      STARTED_TIME: instance.startedTime?.toIso8601String(),
      FINISHED_TIME: instance.finishedTime?.toIso8601String(),
      ROOM: instance.room?.toJSON()
    };

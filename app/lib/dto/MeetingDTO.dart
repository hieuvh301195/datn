import 'package:app/model/Meeting.dart';
import 'package:app/model/Room.dart';
import 'package:json_annotation/json_annotation.dart';

part 'MeetingDTO.g.dart';

@JsonSerializable(explicitToJson: true)
class MeetingDTO{
  final int id;
  final String title;
  final String description;
  final int creatorId;
  final bool isPeriodic;
  final int state;
  final int roomId;
  final DateTime startedTime;
  final DateTime finishedTime;
  final Room room;

  MeetingDTO(
      this.id,
      this.title,
      this.description,
      this.creatorId,
      this.isPeriodic,
      this.state,
      this.roomId,
      this.startedTime,
      this.finishedTime,
      this.room);

  factory MeetingDTO.fromJson(Map <String, dynamic> map) => _$MeetingDTOFromJson(map);

  Map<String, dynamic> toJson () => _$MeetingDTOToJson(this);

  @override
  String toString() {
    return 'MeetingDTO{id: $id, title: $title, description: $description, creatorId: $creatorId, isPeriodic: $isPeriodic, state: $state, roomId: $roomId, startedTime: $startedTime, finishedTime: $finishedTime, room: $room}';
  }
}
import 'package:app/dto/MeetingDTO.dart';
import 'package:app/model/Room.dart';

class MeetingRepository {
  List<MeetingDTO> list = new List();

  List <MeetingDTO> fakeData() {
    List<MeetingDTO> fakeList = new List();
    Room room = new Room(1, 'Phòng khách', true, DateTime.now(), DateTime.now(),
        '18 Tam Trinh', 30);
    fakeList.add(new MeetingDTO(1, "Họp bàn về việc triển khai hệ thống ERP", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(2, "Weekly meeting", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(3, "Họp triển khai vụ golive Bamboo", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(4, "Training cách dùng app NL cho team Vận hành", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(5, "Một số vấn đề tồn động trong hệ thống Ngân Lượng", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(6, "Weekly meeting team Vận Hành", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(7, "Weekly meeting team kinh doanh", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(8, "Bàn về chiến lược kinh doanh tháng 8", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    fakeList.add(new MeetingDTO(9, "Bàn việc tổ chức team building", "", 1,
        false, 1, 1, DateTime.now(), DateTime.now(), room));
    return fakeList;
  }
}

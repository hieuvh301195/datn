import 'package:flutter/material.dart';

class Meeting {
  static const ID = 'id';
  static const TITLE = "title";
  static const DESCRIPTION = "description";
  static const CREATOR_ID = "creator_id";
  static const IS_PERIODIC = "is_periodic";
  static const STATE = "state";
  static const ROOM_ID = "room_id";
  static const STARTED_TIME = "started_time";
  static const FINISHED_TIME = "finished_time";

  final int id;
  final String title;
  final String description;
  final int creatorId;
  final bool isPeriodic;
  final int state;
  final int roomId;
  final DateTime startedTime;
  final DateTime finishedTime;

  Meeting(
      {@required this.id,
      this.title,
      this.description,
      this.creatorId,
      this.isPeriodic,
      this.state,
      this.roomId,
      this.startedTime,
      this.finishedTime});

  Meeting.fromJSON(Map<String, dynamic> map)
      : this(
          id: map[ID],
          title: map[TITLE],
          description: map[DESCRIPTION],
          creatorId: map[CREATOR_ID],
          isPeriodic: map[IS_PERIODIC],
          state: map[STATE],
          roomId: map[ROOM_ID],
          startedTime: map[STARTED_TIME],
          finishedTime: map[FINISHED_TIME],
        );
}

import 'package:flutter/material.dart';

class Employee {
  static const ID = 'id';
  static const FULL_NAME = 'full_name';
  static const EMAIL = 'email';
  static const REFRESH_TOKEN = 'refresh_token';
  static const AVATAR_URL = 'avatar_url';
  static const DEPARTMENT_ID = 'department_id';

  final int id;
  final String fullName;
  final String email;
  final String refreshToken;
  final String avatarUrl;
  final int departmentId;

  Employee(
      {@required this.id,
      this.fullName,
      this.email,
      this.refreshToken,
      this.avatarUrl,
      this.departmentId});

  Employee.fromJSON(Map<String, dynamic> map)
      : this(
          id: map[ID],
          fullName: map[FULL_NAME],
          email: map[EMAIL],
          refreshToken: map[REFRESH_TOKEN],
          avatarUrl: map[AVATAR_URL],
          departmentId: map[DEPARTMENT_ID],
        );

  Map<String, dynamic> toJSON() {
    return {
      ID: id,
      FULL_NAME: fullName,
      EMAIL: email,
      REFRESH_TOKEN: refreshToken,
      AVATAR_URL: avatarUrl,
      DEPARTMENT_ID: departmentId
    };
  }

  @override
  String toString() {
    return 'Employee{id: $id, fullName: $fullName, email: $email, refreshToken: $refreshToken, avatarUrl: $avatarUrl, departmentId: $departmentId}';
  }
}

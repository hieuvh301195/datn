import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';
part 'Role.g.dart';
@JsonSerializable()
class Role{
  static const ID = 'id';
  static const ROLE_TYPE = 'role_type';

  final int id;
  final String roleType;

  Role({@required this.id, this.roleType});

  factory Role.fromJSON(Map <String, dynamic> map) => _$RoleFromJson(map);
  Map<String, dynamic> toJson() => _$RoleToJson(this);
}
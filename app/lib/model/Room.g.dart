// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Room _$RoomFromJson(Map<String, dynamic> json) {
  return Room(
    json['id'] as int,
    json['roomName'] as String,
    json['isMultiAccess'] as bool,
    json['openedTime'] == null
        ? null
        : DateTime.parse(json['openedTime'] as String),
    json['closedTime'] == null
        ? null
        : DateTime.parse(json['closedTime'] as String),
    json['building'] as String,
    json['capacity'] as int,
  );
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'id': instance.id,
      'roomName': instance.roomName,
      'isMultiAccess': instance.isMultiAccess,
      'openedTime': instance.openedTime?.toIso8601String(),
      'closedTime': instance.closedTime?.toIso8601String(),
      'building': instance.building,
      'capacity': instance.capacity,
    };

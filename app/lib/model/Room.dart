import 'package:json_annotation/json_annotation.dart';

part 'Room.g.dart';

@JsonSerializable()
class Room {
  final int id;
  final String roomName;
  final bool isMultiAccess;
  final DateTime openedTime;
  final DateTime closedTime;
  final String building;
  final int capacity;

  Room(this.id, this.roomName, this.isMultiAccess, this.openedTime,
      this.closedTime, this.building, this.capacity);

  factory Room.fromJSON(Map<String, dynamic> map) => _$RoomFromJson(map);

  Map<String, dynamic> toJSON() => _$RoomToJson(this);

}

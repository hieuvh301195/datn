

import 'package:flutter/material.dart';

class CustomColor{
  CustomColor._();

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static final Color primaryColor = fromHex('#fb8732');
  static final Color backgroundColor = fromHex('#DFDFDF');
  static final Color inputBackgroundColor = fromHex('#FFFFFF');
  static final Color normalTextColor = fromHex('#0f2539');
  static final Color normalPlaceholderColor = Color.fromRGBO(15, 37, 57, 0.6);
  static final Color disabledColor = Colors.grey;

}
import 'package:app/navigation/MainNavigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    FlutterStatusbarcolor();
    return (Material(
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'RV-Harmonia'),
        title: 'Flutter Demo',
        routes: MainNavigator.routes,
      ),
    ));
  }
}

package com.example.dao;

import com.example.entity.Meeting;
import com.example.mapper.MeetingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MeetingDAO {

//    private static MeetingDAO instance;
//
//    public static MeetingDAO getInstance() {
//        if (instance == null) {
//            return new MeetingDAO();
//        }
//        return instance;
//    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String queryStatement;

    public List<Meeting> getAllMeetings() {
        queryStatement = "select * from Meeting";
        List<Meeting> result = jdbcTemplate.query(queryStatement, new MeetingMapper());
        System.out.println("result = " + result);
        return result;
    }

    public Meeting getMeetingById(int id) {
        queryStatement = "select * from Meeting where id=?";
        return (Meeting) jdbcTemplate.query(queryStatement, new Object[]{id}, new MeetingMapper());
    }
}

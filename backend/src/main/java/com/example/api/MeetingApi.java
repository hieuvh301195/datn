package com.example.api;

import com.example.dao.MeetingDAO;
import com.example.entity.Meeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MeetingApi {

    @Autowired
    public MeetingDAO meetingDAO;

    @GetMapping("/meeting_list")
    public ResponseEntity<List<Meeting>> getAllMeetings() {
        List<Meeting> result = meetingDAO.getAllMeetings();
        if (result == null) {
            System.out.println("Không có dữ liệu trong db");
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/get_meeting_by_id")
        public ResponseEntity<Meeting> getMeetingById(@RequestParam(value = "meeting_id") int meeting_id) {
        return new ResponseEntity<>(meetingDAO.getMeetingById(meeting_id), HttpStatus.OK);
    }

    @GetMapping("/")
    public String Test() {
        System.out.println("hello");
        return "Hello";
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello() {
        System.out.println("Hit me!");
        return new ResponseEntity<String>("Hello, you!", HttpStatus.OK);
    }
}

package com.example.meeting_project;

import com.example.api.MeetingApi;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
//@ComponentScan (basePackages = "com.example.api")
public class MeetingProjectApplication implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(MeetingProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("ok men");
	}
}


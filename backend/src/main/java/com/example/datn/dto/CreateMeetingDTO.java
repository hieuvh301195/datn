package com.example.datn.dto;

import java.sql.Date;
import java.util.List;

public class CreateMeetingDTO {
    private String meetingTitle;
    private Date startedTime;
    private Date finishedTime;
    private String meetingDescription;
    private int creatorId;
    private int roomId;
}

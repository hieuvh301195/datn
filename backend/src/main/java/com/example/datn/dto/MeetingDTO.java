package com.example.datn.dto;

import com.example.datn.entities.Meeting;
import com.example.datn.entities.Room;

import java.util.List;

public class MeetingDTO {
    private Meeting meeting;
    private EmployeeDTO creator;
    private Room room;
    private List<EmployeeDTO> attendees;

    public MeetingDTO() {
    }

    public MeetingDTO(Meeting meeting, EmployeeDTO creator, Room room, List<EmployeeDTO> attendees) {
        this.meeting = meeting;
        this.creator = creator;
        this.room = room;
        this.attendees = attendees;
    }

    public Meeting getMeeting() {
        return meeting;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    public EmployeeDTO getCreator() {
        return creator;
    }

    public void setCreator(EmployeeDTO creator) {
        this.creator = creator;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<EmployeeDTO> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<EmployeeDTO> attendees) {
        this.attendees = attendees;
    }
}

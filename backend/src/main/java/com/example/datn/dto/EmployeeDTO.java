package com.example.datn.dto;

public class EmployeeDTO {
    private int id;
    private String fullName;
    private String departmentName;
    private String avatarUrl;
    private String email;

    public EmployeeDTO() {
    }

    public EmployeeDTO(int id, String fullName, String departmentName, String avatarUrl, String email) {
        this.id = id;
        this.fullName = fullName;
        this.departmentName = departmentName;
        this.avatarUrl = avatarUrl;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                '}';
    }
}

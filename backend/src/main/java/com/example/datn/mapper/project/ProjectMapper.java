package com.example.datn.mapper.project;

import com.example.datn.entities.Project;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectMapper implements RowMapper<Project> {
    @Override
    public Project mapRow(ResultSet resultSet, int i) throws SQLException {
        Project result = new Project();
        result.setId(resultSet.getInt("id"));
        result.setProjectName(resultSet.getString("project_name"));
        result.setStartedTime(resultSet.getString("started_time"));
        result.setFinishedTime(resultSet.getString("finished_time"));
        result.setEstimatedBudget(resultSet.getFloat("estimated_budget"));
        result.setActualCost(resultSet.getFloat("actual_cost"));
        result.setNumberOfMembers(resultSet.getInt("number_of_members"));
        result.setManagerId(resultSet.getInt("manager_id"));
        return null;
    }
}

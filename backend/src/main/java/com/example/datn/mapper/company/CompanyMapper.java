package com.example.datn.mapper.company;

import com.example.datn.entities.Company;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyMapper implements RowMapper<Company> {
    @Override
    public Company mapRow(ResultSet resultSet, int i) throws SQLException {
        Company result = new Company();
        result.setId(resultSet.getInt("id"));
        result.setCompanyName(resultSet.getString("company_name"));
        result.setLicenseNumber(resultSet.getString("license_number"));
        result.setNumberOfMembers(resultSet.getInt("number_of_members"));
        return null;
    }
}

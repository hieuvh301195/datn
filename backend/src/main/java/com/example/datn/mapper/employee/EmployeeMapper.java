package com.example.datn.mapper.employee;
import com.example.datn.dto.EmployeeDTO;
import com.example.datn.entities.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getInt("id"));
        employee.setFullName(resultSet.getString("full_name"));
        employee.setAvatarUrl(resultSet.getString("avatar_url"));
        employee.setEmail(resultSet.getString("email"));
        employee.setRefreshToken(resultSet.getString("refresh_token"));
        employee.setDepartmentId(resultSet.getInt("department_id"));
        return employee;
    }
}

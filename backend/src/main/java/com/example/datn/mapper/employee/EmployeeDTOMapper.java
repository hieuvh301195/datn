package com.example.datn.mapper.employee;

import com.example.datn.dto.EmployeeDTO;
import com.example.datn.entities.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDTOMapper implements RowMapper<EmployeeDTO> {
    @Override
    public EmployeeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setId(resultSet.getInt("id"));
        employeeDTO.setFullName(resultSet.getString("full_name"));
        employeeDTO.setAvatarUrl(resultSet.getString("avatar_url"));
        employeeDTO.setEmail(resultSet.getString("email"));
        employeeDTO.setDepartmentName(resultSet.getString("department_name"));
        return employeeDTO;
    }
}
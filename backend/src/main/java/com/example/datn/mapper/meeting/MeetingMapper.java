package com.example.datn.mapper.meeting;

import com.example.datn.entities.Meeting;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MeetingMapper implements RowMapper<Meeting> {

    @Override
    public Meeting mapRow(ResultSet resultSet, int i) throws SQLException {
        Meeting meeting = new Meeting();
        meeting.setId(resultSet.getInt("id"));
        meeting.setMeetingTitle(resultSet.getString("meeting_title"));
        meeting.setMeetingDescription(resultSet.getString("meeting_description"));
        meeting.setCreatorId(resultSet.getInt("creator_id"));
        meeting.setPeriodic(resultSet.getBoolean("is_periodic"));
        meeting.setState(resultSet.getInt("state"));
        meeting.setRoomId(resultSet.getInt("room_id"));
        meeting.setStartedTime(resultSet.getString("started_time"));
        meeting.setFinishedTime(resultSet.getString("finished_time"));
        return meeting;
    }

}

package com.example.datn.mapper.room;

import com.example.datn.entities.Room;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoomMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Room room = new Room();
        room.setRoomId(resultSet.getInt("id"));
        room.setRoomName(resultSet.getString("room_name"));
        room.setBuilding(resultSet.getString("building"));
        room.setMultiAccess(resultSet.getBoolean("is_multi_access"));
        room.setOpenedTime(resultSet.getString("opened_time"));
        room.setClosedTime(resultSet.getString("closed_time"));
        room.setCapacity(resultSet.getInt("capacity"));
        return room;
    }
}

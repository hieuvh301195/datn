package com.example.datn.mapper.department;

import com.example.datn.entities.Department;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentMapper implements RowMapper<Department> {
    @Override
    public Department mapRow(ResultSet resultSet, int i) throws SQLException {
        Department result = new Department();
        result.setId(resultSet.getInt("id"));
        result.setDepartmentName(resultSet.getString("department_name"));
        result.setCompanyId(resultSet.getInt("company_id"));
        result.setManagerId(resultSet.getInt("manager_id"));
        result.setNumberOfMembers(resultSet.getInt("number_of_members"));
        return result;
    }
}

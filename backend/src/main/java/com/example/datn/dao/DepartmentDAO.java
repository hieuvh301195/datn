package com.example.datn.dao;

import com.example.datn.entities.Department;
import com.example.datn.mapper.department.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DepartmentDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String sqlStatement;

    public List<Department> getAllDepartment() {
        sqlStatement = "select * from Department";
        return jdbcTemplate.query(sqlStatement, new DepartmentMapper());
    }

    public Department getDepartmentById(int departmentId) {
        sqlStatement = "select * from Department where id = ?";
        return jdbcTemplate.queryForObject(sqlStatement, new Object[]{departmentId}, new DepartmentMapper());
    }

    public List<Department> getAllDepartmentInCompany(int companyId) {
        sqlStatement = "select Department.id, Department.department_name, " +
                "department.number_of_members, Department.manager_id, Department.company_id from Department " +
                "inner join Company on Department.company_id = Company.id where Company.id = ?";
        return jdbcTemplate.query(sqlStatement, new Object[]{companyId}, new DepartmentMapper());
    }
}

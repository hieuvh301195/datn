package com.example.datn.dao;

import com.example.datn.entities.Company;
import com.example.datn.mapper.company.CompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String sqlStatement;

    public List<Company> getAllCompanies() {
        sqlStatement = "select * from Company";
        return jdbcTemplate.query(sqlStatement, new CompanyMapper());
    }

    public Company getCompanyById(int companyId) {
        sqlStatement = "select * from Company where id = ?";
        return jdbcTemplate.queryForObject(sqlStatement, new Object[]{companyId}, new CompanyMapper());
    }
}

package com.example.datn.dao;

import com.example.datn.constant.StatusCode;
import com.example.datn.dto.EmployeeDTO;
import com.example.datn.dto.MeetingDTO;
import com.example.datn.entities.Employee;
import com.example.datn.entities.Meeting;
import com.example.datn.entities.Room;
import com.example.datn.mapper.meeting.MeetingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class MeetingDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RoomDAO roomDAO;

    @Autowired
    private EmployeeDAO employeeDAO;

    private String queryStatement;

    public List<Meeting> getAllMeetings() {
        List<Meeting> result = new ArrayList<>();
        try {
            queryStatement = "select * from Meeting order by Meeting.started_time desc";
            result = jdbcTemplate.query(queryStatement, new MeetingMapper());
            System.out.println("result = " + result);
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return result;
    }

    public List<Meeting> getListMeeting(String date, String email) {
        List<Meeting> result = new ArrayList<>();
        try {
            queryStatement = "select Meeting.id, Meeting.meeting_title, Meeting.meeting_description, " +
                    "Meeting.creator_id, Meeting.is_periodic, Meeting.state, Meeting.room_id, " +
                    "Meeting.started_time, Meeting.finished_time from Meeting " +
                    "inner join MeetingEmployee on Meeting.id = MeetingEmployee.meeting_id " +
                    "inner join Employee on MeetingEmployee.employee_id = Employee.id " +
                    "where date(started_time) = ? and Employee.email = ? ";
            result = jdbcTemplate.query(queryStatement, new Object[]{date, email}, new MeetingMapper());
        } catch (Exception exception) {
            System.out.println(exception.toString());
            exception.printStackTrace();
        }
        return result;
    }

    public Meeting getMeetingById(int id) {
        queryStatement = "select * from Meeting where id=?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{id}, new MeetingMapper());
    }

    public MeetingDTO getMeetingDTOById(int meeting_id) {
        MeetingDTO result = new MeetingDTO();

        Meeting meeting = getMeetingById(meeting_id);
        result.setMeeting(meeting);

        int roomId = meeting.getRoomId();
        Room room = roomDAO.getRoomById(roomId);
        result.setRoom(room);

        EmployeeDTO creator = employeeDAO.getCreatorByMeetingId(meeting_id);
        result.setCreator(creator);

        List<EmployeeDTO> attendees = new ArrayList<>(employeeDAO.getAttendeesByMeetingId(meeting_id));
        result.setAttendees(attendees);

        return result;
    }

    public int createMeetingDTO(MeetingDTO meetingDTO) {
        Meeting meeting = meetingDTO.getMeeting();
        String startedTime = meeting.getStartedTime();
        String finishedTime = meeting.getFinishedTime();

        EmployeeDTO creator = meetingDTO.getCreator();
        Room room = meetingDTO.getRoom();
        int creatorId = employeeDAO.getEmployeeIdByEmail(creator.getEmail());
        System.out.println("creatorId = " + creatorId);
        List<EmployeeDTO> attendees = meetingDTO.getAttendees();
        if (isRoomAvailable(room.getRoomId(), startedTime, finishedTime)) {
            createMeeting(meeting, creatorId, room.getRoomId());
            int meetingId = getMeetingId(room.getRoomId(), creatorId, meeting.getStartedTime());
            System.out.println("meetingId = " + meetingId);
            createCreator(meetingId, creatorId);
            createMeetingEmployee(meetingId, attendees, creatorId);
            return StatusCode.CREATE_MEETING_SUCCESS;
        } else {
            return StatusCode.CREATE_MEETING_FAILED_ROOM_NOT_AVAILABLE;
        }
    }

    public int getMeetingId(int room_id, int creator_id, String started_time) {
        String sql = "Select * from Meeting where creator_id = ? and room_id = ? and started_time = ?";
        Meeting meeting = jdbcTemplate.queryForObject(sql, new Object[]{creator_id, room_id, started_time}, new MeetingMapper());
        if (meeting == null) {
            return -1;
        }
        return meeting.getId();
    }

    public boolean isRoomAvailable(int room_id, String started_time, String finished_time) {
        String sql = "Select * from Meeting where " +
                "room_id = ? and " +
                "started_time > ? and started_time < ? or " +
                "finished_time > ? and finished_time < ? ";
        List<Meeting> result = jdbcTemplate.query(sql, new Object[]{room_id, started_time, finished_time, started_time, finished_time}, new MeetingMapper());
        return result.size() == 0;
    }

    public void createMeeting(Meeting meeting, int creator_id, int room_id) {
        try {
            String sql = "insert into Meeting(meeting_title, meeting_description, " +
                    "creator_id, state, room_id, started_time, finished_time, is_periodic) " +
                    "values(?,?,?,?,?,?,?,?)";
            String title = meeting.getMeetingTitle();
            String description = meeting.getMeetingDescription();
            int state = 2;
            String started_time = meeting.getStartedTime();
            String finished_time = meeting.getFinishedTime();
            boolean is_periodic = meeting.isPeriodic();
            jdbcTemplate.update(sql, title, description, creator_id, state, room_id, started_time, finished_time, is_periodic);
        } catch (Exception exception) {
            System.out.println(exception.toString());
            exception.printStackTrace();
        }
    }

    public void createMeetingEmployee(int meeting_id, List<EmployeeDTO> employees, int creator_id) {

        try {
            for (EmployeeDTO employee : employees) {
                String sql = "insert into MeetingEmployee (meeting_id, employee_id, is_coordinator, may_join) " +
                        "values(?,?,?,?)";
                String employeeEmail = employee.getEmail();
                int employee_id = employeeDAO.getEmployeeIdByEmail(employeeEmail);
                int may_join = 1;
                System.out.println("meeting_id = " + meeting_id);
                System.out.println("employee_id = " + employee_id);
                if (employee_id != creator_id) {
                    jdbcTemplate.update(sql, meeting_id, employee_id, false, may_join);
                }
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
            exception.printStackTrace();
        }
    }

    public void createCreator(int meeting_id, int creator_id) {
        try {
            String sql = "insert into MeetingEmployee (meeting_id, employee_id, is_coordinator, may_join) " +
                    "values(?,?,?,?)";
            int may_join = 1;
            jdbcTemplate.update(sql, meeting_id, creator_id, true, may_join);
        } catch (Exception exception) {
            System.out.println(exception.toString());
            exception.printStackTrace();
        }
    }

}

package com.example.datn.dao;

import com.example.datn.dto.EmployeeDTO;
import com.example.datn.entities.Employee;
import com.example.datn.mapper.employee.EmployeeDTOMapper;
import com.example.datn.mapper.employee.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String queryStatement;

    public List<Employee> getAllEmployees() {
        List<Employee> result = new ArrayList<>();
        try {
            queryStatement = "select * from Employee";
            result = jdbcTemplate.query(queryStatement, new EmployeeMapper());
            System.out.println("result = " + result);
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return result;
    }

    public Employee getEmployeeById(int id) {
        queryStatement = "select * from Employee where id=?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{id}, new EmployeeMapper());
    }

    public Employee getEmployeeByEmail(String email){
        queryStatement = "Select * from Employee where email = ?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{email}, new EmployeeMapper());
    }

    public EmployeeDTO getEmployeeDTOByEmail(String email) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "where Employee.email=?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{email}, new EmployeeDTOMapper());
    }

    public int getEmployeeIdByEmail(String email) {
        EmployeeDTO employeeDTO = getEmployeeDTOByEmail(email);
        return employeeDTO.getId();
    }

    public EmployeeDTO getEmployeeDTOById(int id) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "where Employee.id=?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{id}, new EmployeeDTOMapper());
    }

    public EmployeeDTO getCreatorByMeetingId(int meeting_id) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "inner join Meeting on Employee.id = Meeting.creator_id " +
                "where Meeting.id=?";
        return jdbcTemplate.queryForObject(queryStatement, new Object[]{meeting_id}, new EmployeeDTOMapper());
    }

    public List<EmployeeDTO> getAttendeesByMeetingId(int meeting_id) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "inner join MeetingEmployee on Employee.id = MeetingEmployee.employee_id " +
                "inner join Meeting on MeetingEmployee.meeting_id = Meeting.id " +
                "where Meeting.id = ?";
        return jdbcTemplate.query(queryStatement, new Object[]{meeting_id}, new EmployeeDTOMapper());
    }

    public List<EmployeeDTO> getEmployeesDTOByProjectId(int projectId) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, " +
                "Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "inner join ProjectEmployee on Employee.id = ProjectEmployee.employee_id " +
                "inner join Project on ProjectEmployee.project_id = Project.id " +
                "where Project.id = ?";
        return jdbcTemplate.query(queryStatement, new Object[]{projectId}, new EmployeeDTOMapper());
    }


    public List<EmployeeDTO> getEmployeesDTOByDepartmentId(int departmentId) {
        queryStatement = "select Employee.id, Employee.full_name, Employee.email, " +
                "Employee.avatar_url, Department.department_name " +
                "from Employee " +
                "inner join Department on Employee.department_id = Department.id " +
                "where Department.id = ?";
        return jdbcTemplate.query(queryStatement, new Object[]{departmentId}, new EmployeeDTOMapper());
    }

    public boolean loginByEmail(Employee employeeRequest) {
        String email = employeeRequest.getEmail();
        String querySql = "select * from Employee where email = ?";
        Employee employee = jdbcTemplate.queryForObject(querySql, new Object[]{email}, new EmployeeMapper());
        if (employee != null) {
            String updateSql = "update Employee " +
                    "set refresh_token = ?, avatar_url = ? " +
                    "where id = ?";
            jdbcTemplate.update(updateSql, employeeRequest.getRefreshToken(), employeeRequest.getAvatarUrl(), employee.getId());
            return true;
        }
        return false;
    }
}

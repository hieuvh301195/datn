package com.example.datn.dao;

import com.example.datn.entities.Project;
import com.example.datn.mapper.project.ProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProjectDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private String sqlStatement;

    public List<Project> getAllProjects(){
        sqlStatement = "select * from Project";
        return jdbcTemplate.query(sqlStatement, new ProjectMapper());
    }

    public Project getProjectById(int projectId){
        sqlStatement = "select * from Project where id = ?";
        return jdbcTemplate.queryForObject(sqlStatement, new Object[]{projectId}, new ProjectMapper());
    }
}

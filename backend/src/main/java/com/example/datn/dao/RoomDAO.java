package com.example.datn.dao;

import com.example.datn.entities.Room;
import com.example.datn.mapper.room.RoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RoomDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String queryStatement;

    public List<Room> getAllRoom() {
        List<Room> result = new ArrayList<>();
        try {
            queryStatement = "select * from Room";
            result = jdbcTemplate.query(queryStatement, new RoomMapper());
            System.out.println("result = " + result);
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return result;
    }

    public Room getRoomById(int id) {
        queryStatement = "select * from Room where id=?";
        return (Room) jdbcTemplate.queryForObject(queryStatement, new Object[]{id}, new RoomMapper());
    }
}

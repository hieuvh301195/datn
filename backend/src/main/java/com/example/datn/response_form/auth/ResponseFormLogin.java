package com.example.datn.response_form.auth;

public class ResponseFormLogin {
    private int code;
    private String message;
    private long request_time;
    private String access_token;

    public ResponseFormLogin(int code, String message, long request_time, String access_token) {
        this.code = code;
        this.message = message;
        this.request_time = request_time;
        this.access_token = access_token;
    }

    public ResponseFormLogin() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getRequest_time() {
        return request_time;
    }

    public void setRequest_time(long request_time) {
        this.request_time = request_time;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public String toString() {
        return "ResponseFormLogin{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", request_time=" + request_time +
                ", access_token='" + access_token + '\'' +
                '}';
    }
}

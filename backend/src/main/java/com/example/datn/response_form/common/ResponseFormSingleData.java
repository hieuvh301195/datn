package com.example.datn.response_form.common;

import com.example.datn.dto.MeetingDTO;

import java.util.List;

public class ResponseFormSingleData<Object> {
    private String message;
    private Object data;
    private int code;
    private long requestTime = System.currentTimeMillis();

    public ResponseFormSingleData(String message, Object data, int code) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResponseFormSingleData(){ }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseFormSingleData{" +
                "message='" + message + '\'' +
                ", data=" + data +
                ", code=" + code +
                ", requestTime=" + requestTime +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

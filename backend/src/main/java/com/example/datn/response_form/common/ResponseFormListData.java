package com.example.datn.response_form.common;

import java.util.List;

public class ResponseFormListData<Object> {
    private int code;
    private String message;
    private List<Object> data;
    private long requestTime = System.currentTimeMillis();

    public ResponseFormListData(){};

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public ResponseFormListData(String message, List<Object> data, int code) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseFormListData{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }
}

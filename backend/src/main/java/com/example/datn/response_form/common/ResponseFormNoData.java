package com.example.datn.response_form.common;

public class ResponseFormNoData {
    private int code;
    private String message;
    private long requestTime = System.currentTimeMillis();

    public ResponseFormNoData(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseFormNoData() { }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }
}

package com.example.datn.constant;

public class StatusCode {
    public static final int UNDEFINED_ERROR = -1;
    public static final int SUCCESSFUL_CODE = 0;
    public static final int MISSING_PARAM = 1;
    public static final int INVALID_INFO = 2;
    public static final int LOGIN_FAILED = 3;
    public static final int CREATE_MEETING_SUCCESS = 4;
    public static final int CREATE_MEETING_FAILED_ROOM_NOT_AVAILABLE = 5;
}

package com.example.datn.entities;

public class Department{

    private int id;
    private String departmentName;
    private int numberOfMembers;
    private int managerId;
    private int companyId;


    public Department(int id, String departmentName, int numberOfMembers, int managerId, int companyId) {
        this.id = id;
        this.departmentName = departmentName;
        this.numberOfMembers = numberOfMembers;
        this.managerId = managerId;
        this.companyId = companyId;

    }

    public Department() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setNumberOfMembers(int numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }


    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", departmentName='" + departmentName + '\'' +
                ", numberOfMembers=" + numberOfMembers +
                ", managerId=" + managerId +
                ", companyId=" + companyId +
                '}';
    }
}

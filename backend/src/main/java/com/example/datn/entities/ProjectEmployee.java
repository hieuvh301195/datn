package com.example.datn.entities;

public class ProjectEmployee {
    private int projectId;
    private int employeeId;
    private String joinTime;
    private String ejectTime;
    private String roleInProject;

    public ProjectEmployee() {
    }

    public ProjectEmployee(int projectId, int employeeId, String joinTime, String ejectTime, String roleInProject) {
        this.projectId = projectId;
        this.employeeId = employeeId;
        this.joinTime = joinTime;
        this.ejectTime = ejectTime;
        this.roleInProject = roleInProject;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getEjectTime() {
        return ejectTime;
    }

    public void setEjectTime(String ejectTime) {
        this.ejectTime = ejectTime;
    }

    public String getRoleInProject() {
        return roleInProject;
    }

    public void setRoleInProject(String roleInProject) {
        this.roleInProject = roleInProject;
    }
}

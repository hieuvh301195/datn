package com.example.datn.entities;

public class Device {
    private int id;
    private String deviceName;
    private String buyTime;
    private String liquidationTime;
    private float buyPrice;
    private float liquidationPrice;
    private int roomId;
    private int numberOfDevices;
    private String specifications;
    private int status;

    public Device() {
    }

    public Device(int id, String deviceName, String buyTime, String liquidationTime, float buyPrice, float liquidationPrice, int roomId, int numberOfDevices, String specifications, int status) {
        this.id = id;
        this.deviceName = deviceName;
        this.buyTime = buyTime;
        this.liquidationTime = liquidationTime;
        this.buyPrice = buyPrice;
        this.liquidationPrice = liquidationPrice;
        this.roomId = roomId;
        this.numberOfDevices = numberOfDevices;
        this.specifications = specifications;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public String getLiquidationTime() {
        return liquidationTime;
    }

    public void setLiquidationTime(String liquidationTime) {
        this.liquidationTime = liquidationTime;
    }

    public float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(float buyPrice) {
        this.buyPrice = buyPrice;
    }

    public float getLiquidationPrice() {
        return liquidationPrice;
    }

    public void setLiquidationPrice(float liquidationPrice) {
        this.liquidationPrice = liquidationPrice;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getNumberOfDevices() {
        return numberOfDevices;
    }

    public void setNumberOfDevices(int numberOfDevices) {
        this.numberOfDevices = numberOfDevices;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

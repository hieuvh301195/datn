package com.example.datn.entities;

public class Company {
    private int id;
    private String companyName;
    private String licenseNumber;
    private int numberOfMembers;

    public Company(int id, String companyName, String licenseNumber, int numberOfMembers) {
        this.id = id;
        this.companyName = companyName;
        this.licenseNumber = licenseNumber;
        this.numberOfMembers = numberOfMembers;
    }

    public Company() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setNumberOfMembers(int numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", numberOfMembers=" + numberOfMembers +
                '}';
    }
}

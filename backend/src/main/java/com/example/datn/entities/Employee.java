package com.example.datn.entities;

public class Employee{

    private int id;
    private String fullName;
    private String email;
    private String refreshToken;
    private String avatarUrl;
    private int departmentId;

    public Employee(){}

    public Employee(int id, String fullName, String email, String refreshToken, String avatarUrl, int departmentId) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.refreshToken = refreshToken;
        this.avatarUrl = avatarUrl;
        this.departmentId = departmentId;
    }

    public Employee(String fullName, String email, String refreshToken, String avatarUrl) {
        this.fullName = fullName;
        this.email = email;
        this.refreshToken = refreshToken;
        this.avatarUrl = avatarUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", departmentId=" + departmentId +
                '}';
    }
}
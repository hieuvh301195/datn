package com.example.datn.entities;

public class RoleEmployee {
    private int roleId;
    private int employeeId;

    public RoleEmployee(int roleId, int employeeId) {
        this.roleId = roleId;
        this.employeeId = employeeId;
    }

    public RoleEmployee() {
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
}

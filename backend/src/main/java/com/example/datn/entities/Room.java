package com.example.datn.entities;

public class Room {
    private int roomId;
    private String roomName;
    private boolean isMultiAccess;
    private String openedTime;
    private String closedTime;
    private String building;
    private int capacity;

    public Room() {
    }

    public Room(int roomId, String roomName, boolean isMultiAccess, String openedTime, String closedTime, String building, int capacity) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.isMultiAccess = isMultiAccess;
        this.openedTime = openedTime;
        this.closedTime = closedTime;
        this.building = building;
        this.capacity = capacity;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public boolean isMultiAccess() {
        return isMultiAccess;
    }

    public void setMultiAccess(boolean multiAccess) {
        isMultiAccess = multiAccess;
    }

    public String getOpenedTime() {
        return openedTime;
    }

    public void setOpenedTime(String openedTime) {
        this.openedTime = openedTime;
    }

    public String getClosedTime() {
        return closedTime;
    }

    public void setClosedTime(String closedTime) {
        this.closedTime = closedTime;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}

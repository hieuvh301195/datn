package com.example.datn.entities;

public class Role {
    private int id;
    private String roleType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public Role(int id, String roleType) {
        this.id = id;
        this.roleType = roleType;
    }

    public Role() {}

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", roleType='" + roleType + '\'' +
                '}';
    }
}

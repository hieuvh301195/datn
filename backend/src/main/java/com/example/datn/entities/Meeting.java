package com.example.datn.entities;

import java.sql.Date;

public class Meeting {
    private int id;
    private String meetingTitle;
    private String meetingDescription;
    private int creatorId;
    private boolean isPeriodic;
    private int state;
    private int roomId;
    private String startedTime;
    private String finishedTime;

    public Meeting() {
    }

    public Meeting(int id, String meetingTitle, String meetingDescription, int creatorId, boolean isPeriodic, int state, int roomId, String startedTime, String finishedTime) {
        this.id = id;
        this.meetingTitle = meetingTitle;
        this.meetingDescription = meetingDescription;
        this.creatorId = creatorId;
        this.isPeriodic = isPeriodic;
        this.state = state;
        this.roomId = roomId;
        this.startedTime = startedTime;
        this.finishedTime = finishedTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeetingTitle() {
        return meetingTitle;
    }

    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle;
    }

    public String getMeetingDescription() {
        return meetingDescription;
    }

    public void setMeetingDescription(String meetingDescription) {
        this.meetingDescription = meetingDescription;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public boolean isPeriodic() {
        return isPeriodic;
    }

    public void setPeriodic(boolean periodic) {
        isPeriodic = periodic;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getStartedTime() {
        return startedTime;
    }

    public void setStartedTime(String startedTime) {
        this.startedTime = startedTime;
    }

    public String getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(String finishedTime) {
        this.finishedTime = finishedTime;
    }
}

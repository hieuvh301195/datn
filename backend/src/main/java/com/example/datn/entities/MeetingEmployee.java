package com.example.datn.entities;

public class MeetingEmployee {
    private int meetingId;
    private int employeeId;
    private boolean isCoordinator;
    private int mayJoin;

    public MeetingEmployee(int meetingId, int employeeId, boolean isCoordinator, int mayJoin) {
        this.meetingId = meetingId;
        this.employeeId = employeeId;
        this.isCoordinator = isCoordinator;
        this.mayJoin = mayJoin;
    }

    public MeetingEmployee() {
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public boolean isCoordinator() {
        return isCoordinator;
    }

    public void setCoordinator(boolean coordinator) {
        isCoordinator = coordinator;
    }

    public int getMayJoin() {
        return mayJoin;
    }

    public void setMayJoin(int mayJoin) {
        this.mayJoin = mayJoin;
    }

    @Override
    public String toString() {
        return "MeetingEmployee{" +
                "meetingId=" + meetingId +
                ", employeeId=" + employeeId +
                ", isCoordinator=" + isCoordinator +
                ", mayJoin=" + mayJoin +
                '}';
    }
}

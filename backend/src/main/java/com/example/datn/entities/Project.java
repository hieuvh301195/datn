package com.example.datn.entities;

public class Project {
    private int id;
    private String projectName;
    private String startedTime;
    private String finishedTime;
    private float estimatedBudget;
    private float actualCost;
    private int numberOfMembers;
    private int managerId;

    public Project() {
    }

    public Project(int id, String projectName, String startedTime, String finishedTime, float estimatedBudget, float actualCost, int numberOfMembers, int managerId) {
        this.id = id;
        this.projectName = projectName;
        this.startedTime = startedTime;
        this.finishedTime = finishedTime;
        this.estimatedBudget = estimatedBudget;
        this.actualCost = actualCost;
        this.numberOfMembers = numberOfMembers;
        this.managerId = managerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getStartedTime() {
        return startedTime;
    }

    public void setStartedTime(String startedTime) {
        this.startedTime = startedTime;
    }

    public String getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(String finishedTime) {
        this.finishedTime = finishedTime;
    }

    public float getEstimatedBudget() {
        return estimatedBudget;
    }

    public void setEstimatedBudget(float estimatedBudget) {
        this.estimatedBudget = estimatedBudget;
    }

    public float getActualCost() {
        return actualCost;
    }

    public void setActualCost(float actualCost) {
        this.actualCost = actualCost;
    }

    public int getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setNumberOfMembers(int numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", projectName='" + projectName + '\'' +
                ", startedTime='" + startedTime + '\'' +
                ", finishedTime='" + finishedTime + '\'' +
                ", estimatedBudget=" + estimatedBudget +
                ", actualCost=" + actualCost +
                ", numberOfMembers=" + numberOfMembers +
                ", managerId=" + managerId +
                '}';
    }
}

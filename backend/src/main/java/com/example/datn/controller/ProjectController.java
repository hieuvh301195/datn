package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.ProjectDAO;
import com.example.datn.entities.Project;
import com.example.datn.entities.Room;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {
    @Autowired
    private ProjectDAO projectDAO;

    @GetMapping("project_list")
    public ResponseEntity<ResponseFormListData<Project>> getAllRooms(){
        ResponseFormListData<Project> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setData(projectDAO.getAllProjects());
        response.setMessage("Thành công");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("project/{project_id}")
    public ResponseEntity<ResponseFormSingleData<Project>> getProjectById(@PathVariable int project_id){
        ResponseFormSingleData<Project> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setData(projectDAO.getProjectById(project_id));
        response.setMessage("Thành công");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.DepartmentDAO;
import com.example.datn.entities.Department;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {
    @Autowired
    private DepartmentDAO departmentDAO;

    @GetMapping("department_list")
    public ResponseEntity<ResponseFormListData<Department>> getAllDepartment(){
        ResponseFormListData<Department> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(departmentDAO.getAllDepartment());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("department/{department_id}")
    public ResponseEntity<ResponseFormSingleData<Department>> getDepartmentById(@PathVariable int department_id){
        ResponseFormSingleData<Department> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(departmentDAO.getDepartmentById(department_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("department_by_company_id/{company_id}")
    public ResponseEntity<ResponseFormListData<Department>> getAllDepartmentInCompany(@PathVariable int company_id){
        ResponseFormListData<Department> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(departmentDAO.getAllDepartmentInCompany(company_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

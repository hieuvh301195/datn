package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.MeetingDAO;
import com.example.datn.dto.MeetingDTO;
import com.example.datn.entities.Meeting;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormNoData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MeetingController {

    @Autowired
    private MeetingDAO meetingDAO;

    @GetMapping("/")
    public String Hello() {
        return "Hellssso";
    }

    @GetMapping("/meeting_list")
    public ResponseEntity<ResponseFormListData<Meeting>>
    getAllMeetings(@RequestParam(value = "date", required = false) String date,
                   @RequestParam(value = "email", required = false) String email) {
        ResponseFormListData<Meeting> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        if (date != null && email != null) {
            response.setData(meetingDAO.getListMeeting(date, email));
        } else {
            response.setData(meetingDAO.getAllMeetings());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/meeting/{meeting_id}")
    public ResponseEntity<ResponseFormSingleData<MeetingDTO>> getMeetingDTOById(@PathVariable int meeting_id) {
        ResponseFormSingleData<MeetingDTO> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(meetingDAO.getMeetingDTOById(meeting_id));
        System.out.println("response getMeetingById = " + response.toString());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/meeting")
    public ResponseEntity<ResponseFormNoData> createMeeting(@RequestBody(required = true) MeetingDTO meetingDTO) {
        ResponseFormNoData response = new ResponseFormNoData();
        int result = meetingDAO.createMeetingDTO(meetingDTO);
        switch (result) {
            case StatusCode.CREATE_MEETING_SUCCESS: {
                response.setCode(StatusCode.SUCCESSFUL_CODE);
                response.setMessage("Thành công");
                break;
            }
            case StatusCode.CREATE_MEETING_FAILED_ROOM_NOT_AVAILABLE: {
                response.setCode(StatusCode.CREATE_MEETING_FAILED_ROOM_NOT_AVAILABLE);
                response.setMessage("Phòng họp đã được sử dụng trong khung thời gian này");
                break;
            }
            case StatusCode.UNDEFINED_ERROR:{
                response.setCode(StatusCode.UNDEFINED_ERROR);
                response.setMessage("Lỗi không xác định");
                break;
            }
        }
        System.out.println("response getMeetingById = " + response.toString());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

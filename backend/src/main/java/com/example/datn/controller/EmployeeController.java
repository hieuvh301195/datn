package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.EmployeeDAO;
import com.example.datn.dto.EmployeeDTO;
import com.example.datn.entities.Employee;
import com.example.datn.response_form.auth.ResponseFormLogin;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeDAO employeeDAO;

    @GetMapping("/employee_list")
    public ResponseEntity<ResponseFormListData<Employee>> getAllEmployees() {
        ResponseFormListData<Employee> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getAllEmployees());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/meeting/{meeting_id}/attendees_list/")
    public ResponseEntity<ResponseFormListData<EmployeeDTO>> getAttendeesByMeetingId(@PathVariable int meeting_id) {
        ResponseFormListData<EmployeeDTO> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getAttendeesByMeetingId(meeting_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/employee/get_employee_by_id/{employee_id}")
    public ResponseEntity<ResponseFormSingleData<EmployeeDTO>> getEmployeeById(@PathVariable int employee_id) {
        ResponseFormSingleData<EmployeeDTO> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getEmployeeDTOById(employee_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/employee/get_employee_by_email/{email}")
    public ResponseEntity<ResponseFormSingleData<EmployeeDTO>> getEmployeeByEmail(@PathVariable String email) {
        ResponseFormSingleData<EmployeeDTO> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getEmployeeDTOByEmail(email));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/employee/get_employees_by_project_id/{project_id}")
    public ResponseEntity<ResponseFormListData<EmployeeDTO>> getEmployeeByProjectId(@PathVariable int project_id) {
        ResponseFormListData<EmployeeDTO> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getEmployeesDTOByProjectId(project_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/employee/get_employees_by_department_id/{department_id}")
    public ResponseEntity<ResponseFormListData<EmployeeDTO>> getEmployeeByDepartmentId(@PathVariable int department_id) {
        ResponseFormListData<EmployeeDTO> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(employeeDAO.getEmployeesDTOByDepartmentId(department_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/employee/login")
    public ResponseEntity<ResponseFormLogin> login(@ModelAttribute Employee employee) {
        ResponseFormLogin response = new ResponseFormLogin();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setAccess_token("458770FE94578E3EBFF60FCD812AF684");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

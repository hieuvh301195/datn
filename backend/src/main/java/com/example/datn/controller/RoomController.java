package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.RoomDAO;
import com.example.datn.entities.Room;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {
    @Autowired
    private RoomDAO roomDAO;

    @GetMapping("room_list")
    public ResponseEntity<ResponseFormListData<Room>> getAllRooms(){
        ResponseFormListData<Room> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setData(roomDAO.getAllRoom());
        response.setMessage("Thành công");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("room/{room_id}")
    public ResponseEntity<ResponseFormSingleData<Room>> getRoomById(@PathVariable int room_id){
        ResponseFormSingleData<Room> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setData(roomDAO.getRoomById(room_id));
        response.setMessage("Thành công");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

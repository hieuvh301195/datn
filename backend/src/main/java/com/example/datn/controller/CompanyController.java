package com.example.datn.controller;

import com.example.datn.constant.StatusCode;
import com.example.datn.dao.CompanyDAO;
import com.example.datn.entities.Company;
import com.example.datn.response_form.common.ResponseFormListData;
import com.example.datn.response_form.common.ResponseFormSingleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyController {
    @Autowired
    private CompanyDAO companyDAO;

    @GetMapping("company_list")
    public ResponseEntity<ResponseFormListData<Company>> getAllCompany(){
        ResponseFormListData<Company> response = new ResponseFormListData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(companyDAO.getAllCompanies());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("company/{company_id}")
    public ResponseEntity<ResponseFormSingleData<Company>> getCompanyById(@PathVariable int company_id){
        ResponseFormSingleData<Company> response = new ResponseFormSingleData<>();
        response.setCode(StatusCode.SUCCESSFUL_CODE);
        response.setMessage("Thành công");
        response.setData(companyDAO.getCompanyById(company_id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

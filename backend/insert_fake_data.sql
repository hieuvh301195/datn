use meeting_project;

-- role
insert into Role (role_type) value ("admin");  
insert into Role (role_type) value ("standard"); 
insert into Role (role_type) value ("root");
insert into Role (role_type) value ("developer");
insert into Role (role_type) value ("maintainer");   

-- room

insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng khách", true, "08:30", "17:30", "18 Tam Trinh", 20); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng học", true, "08:30", "17:30", "18 Tam Trinh", 25); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng đổi", true, "08:30", "17:30", "18 Tam Trinh", 40); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng chủ", true, "08:30", "17:30", "18 Tam Trinh", 5); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng trung", true, "08:30", "17:30", "20 Tam Trinh", 20); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng sáng", true, "08:30", "17:30", "20 Tam Trinh", 30); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng nhóm", true, "08:30", "17:30", "20 Tam Trinh", 30); 
insert into Room(room_name, is_multi_access, opened_time, closed_time, building, capacity) 
value("Phòng siêng", true, "08:30", "17:30", "18 Tam Trinh", 50); 

-- deivice
-- room1  
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 1, 4, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 1, 20, 2); 

-- room2
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 2, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 2, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 2, 4, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 2, 25, 2); 

-- room3
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 3, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 3, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 3, 4, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 3, 40, 2); 

-- room4
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 4, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 4, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 4, 4, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 4, 5, 2); 

-- room5
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 5, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 5, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 5, 4, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 5, 20, 2); 

-- room6
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 6, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 6, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 6, 20, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null,6, 50, 2); 

-- room7
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 7, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null, 7, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 7, 10, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 7, 30, 2); 

-- room8
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Máy Chiếu Sony VPL-EX430", "21-12-2019", null, 13899000, null, 8, 1 ,2 ); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Màn chiếu treo tường Dalite P80WS ( 2m03x2m03 ) - 113 inch", "21-12-2019", null, 849000, null,8, 1, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Bàn khung sắt BUD02", "21-12-2019", null, 500000, null, 8, 10, 2); 
insert into Device(device_name, buy_time, liquidation_time, buy_price, liquidation_price, room_id, number_devices, device_status) 
value("Ghế chân quỳ Q1", "21-12-2019", null, 13899000, null, 8, 30, 2); 

-- company
insert into Company(company_name, license_number, number_of_members)
value ("Công ty cổ phần Ngân Lượng", 011032001676, 192); 
insert into Company(company_name, license_number, number_of_members)
value ("Công ty cổ phần Vimo", 011033501125, 122); 
insert into Company(company_name, license_number, number_of_members)
value ("Công ty cổ phần FastGo Việt Nam", 0108253207, 53); 
insert into Company(company_name, license_number, number_of_members)
value ("Công ty cổ phần công nghệ mPOS Việt Nam", 0106146986, 153); 

-- department
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kỹ thuật", 22,1  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kinh doanh", 50, 1 ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kế toán", 10,1  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng hành chính công vụ", 8, 1 ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng hỗ trợ khách hàng nước ngoài", 30, 1 ); 

insert into Department(department_name, number_of_members, company_id) 
value("Phòng kỹ thuật", 22,2  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kinh doanh", 50, 2 ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kế toán", 10,2  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng hành chính công vụ", 8, 2 ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng hỗ trợ khách hàng nước ngoài", 30,2 ); 

insert into Department(department_name, number_of_members, company_id) 
value("Phòng kỹ thuật", 22,3  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kinh doanh", 50, 3 ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng kế toán", 10,3  ); 
insert into Department(department_name, number_of_members, company_id) 
value("Phòng hành chính công vụ", 8, 3 ); 

insert into Employee(full_name, email, department_id)
value("Vũ Huy Hiếu", "hieu.vh301195@gmail.com",  1);
insert into Employee(full_name, email, department_id)
value("Hà Văn Linh", "linhhv@nganluong.vn", 1);
insert into Employee(full_name, email, department_id)
value("Bùi Thanh Hải", "haibt@peacesoft.net", 3);
insert into Employee(full_name, email, department_id)
value("Nguyễn Mạnh Đức", "ducnm@nganluong.vn", 4);
insert into Employee(full_name, email, department_id)
value("Vũ Thị Quy", "quyvt@nganluong.vn", 2);
insert into Employee(full_name, email, department_id)
value("Hoàng Văn Linh", "linhhv@peacesoft.net",3);
insert into Employee(full_name, email, department_id)
value("Nguyễn Cẩm Huế", "huenc@peacesoft.net",1);
insert into Employee(full_name, email, department_id)
value("Ngô Đình Tài", "taidn@nganluong.vn", 2);
insert into Employee(full_name, email, department_id)
value("Nguyễn Thi Vân", "vannt@peacesoft.net",1);
insert into Employee(full_name, email, department_id)
value("Nguyễn Thị Lan Nga", "ngantl@nganluong.vn",1);
insert into Employee(full_name, email, department_id)
value("Nguyễn Mạnh Duy", "duynm@nganluong.vn",1);
insert into Employee(full_name, email, department_id)
value("Nguyễn Thu Trà", "trant@nganluong.vn",5);
insert into Employee(full_name, email, department_id)
value("Nguyễn Bá Duy", "duynb@vimo.vn",6);
insert into Employee(full_name, email, department_id)
value("Trần Nguyên Anh", "anhtn@nganluong.vn",2);
insert into Employee(full_name, email, department_id)
value("Nguyễn Hải Đăng", "dangnh@vimo.vn",6);
insert into Employee(full_name, email, department_id)
value("Nguyễn Mạnh Tuất", "tuatmn@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Bùi Đình Tài", "taibd@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Nguyễn Văn Sơn", "sonnv@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Nguyễn Bá Mạnh", "manhnb@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Đỗ Đình Tài", "taidd@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Nguyễn Thị Hồng Nga", "nganth@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Đỗ Thế Nghiêm", "nghiemdt@fastgo.vn",8);
insert into Employee(full_name, email, department_id)
value("Triệu Khánh Hòa", "hoatk@fastgo.vn",7);
insert into Employee(full_name, email, department_id)
value("Triệu Thị Định", "dinhtt@fastgo.vn",7);
insert into Employee(full_name, email, department_id)
value("Vũ Huy Hiếu", "hieuvh@nganluong.vn",1);
insert into Employee(full_name, email, department_id)
value("Vũ Huy Hiếu", "vuhuyhieu2@gmail.com",2);
insert into Employee(full_name, email, department_id)
value("Vũ Huy Hiếu", "hieubeobb@gmail.com",3);



insert into RoleEmployee(employee_id, role_id) values (1,1); 
insert into RoleEmployee(employee_id, role_id) values (1,2); 
insert into RoleEmployee(employee_id, role_id) values (1,3); 
insert into RoleEmployee(employee_id, role_id) values (2,2); 
insert into RoleEmployee(employee_id, role_id) values (3,2); 
insert into RoleEmployee(employee_id, role_id) values (4,2); 
insert into RoleEmployee(employee_id, role_id) values (5,2); 
insert into RoleEmployee(employee_id, role_id) values (6,2); 
insert into RoleEmployee(employee_id, role_id) values (7,2); 
insert into RoleEmployee(employee_id, role_id) values (8,2); 
insert into RoleEmployee(employee_id, role_id) values (1,4); 
insert into RoleEmployee(employee_id, role_id) values (1,5); 
insert into RoleEmployee(employee_id, role_id) values (2,1); 
insert into RoleEmployee(employee_id, role_id) values (3,1); 
insert into RoleEmployee(employee_id, role_id) values (9,2); 
insert into RoleEmployee(employee_id, role_id) values (10,2); 
insert into RoleEmployee(employee_id, role_id) values (11,2);  
insert into RoleEmployee(employee_id, role_id) values (12,2); 
insert into RoleEmployee(employee_id, role_id) values (13,2); 
insert into RoleEmployee(employee_id, role_id) values (14,2);  
insert into RoleEmployee(employee_id, role_id) values (15,2);  
insert into RoleEmployee(employee_id, role_id) values (16,2); 
insert into RoleEmployee(employee_id, role_id) values (17,2); 
insert into RoleEmployee(employee_id, role_id) values (18,2);  
insert into RoleEmployee(employee_id, role_id) values (19,2); 
insert into RoleEmployee(employee_id, role_id) values (20,2);  
insert into RoleEmployee(employee_id, role_id) values (21,2); 
insert into RoleEmployee(employee_id, role_id) values (18,1);  
insert into RoleEmployee(employee_id, role_id) values (19,1); 
insert into RoleEmployee(employee_id, role_id) values (20,1);  
insert into RoleEmployee(employee_id, role_id) values (21,3); 

-- meeting
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn triển khai dự án thu học phí", 2, false, 1, 4, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn golive app Ví Ngân Lượng", 1, false, 1, 4, "2020-07-25T10:30:00.099", "2020-07-25T11:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn việc thay đổi công nghệ phía client", 2, false, 1, 5, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp team marketing", 5, false, 1, 8, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp team Vận Hành Ngân Lượng", 4, false, 1, 5, "2020-07-26T08:30:00.099", "2020-07-26T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn triển khai dự án Bamboo Airway", 2, false, 1, 4, "2020-07-26T08:30:00.099", "2020-07-26T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp tổng kết quý II Ngân Lượng", 8, false, 1, 6, "2020-07-26T08:30:00.099", "2020-07-26T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp về vụ nhận lại trang nganluong.vn", 1, false, 1, 6, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp về vụ cải tiến IP", 2, false, 1, 7, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn một số vấn đề của app Ngân Lượng", 2, false, 1, 6, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Họp bàn về khủng hoảng marketing FastGo", 9, false, 1, 3, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Training cách sử dụng app Ngân Lượng", 2, false, 1, 3, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 
insert into Meeting(meeting_title, creator_id, is_periodic, state, room_id, started_time, finished_time)
values ("Training về PCI-DSS cho tập đoàn", 2, false, 1, 3, "2020-07-25T08:30:00.099", "2020-07-25T09:30:00.099"); 

-- meeting employee
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,1,1,true);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,2,2,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,3,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,4,3,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,5,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,6,2,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(1,8,2,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(2,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(3,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(4,2,1,true);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(4,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(3,2,2,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(5,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(6,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(7,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(8,1,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(2,2,3,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(3,8,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(5,3,3,true);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(2,3,2,true);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(7,3,2,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(8,3,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(9,3,1,false);
insert into MeetingEmployee(employee_id, meeting_id, may_join, is_coordinator) 
values(10,3,1,false);
